const router = require('express').Router()
const HtmlAction = require('actions/HtmlAction')
const JsonAction = require('actions/JsonAction')
const ComponentService = require('services/ComponentService')
const TwitterService = require('services/TwitterService')
const maybeLogin = require('helpers/Auth0Middleware').maybeLogin
const moduleMeta = require('helpers/moduleMeta')
const urlsafeBase64 = require('urlsafe-base64')

const send = (req, res, data) => {

  Object.assign(data, {layout: 'layouts/page'})

  const html = () => (new HtmlAction(req, res)).render(data)
  const json = () => (new JsonAction(req, res)).render(data)

  const negotiation = {'default': html}

  req.isAuthenticated() || Object.assign(negotiation, {
    'text/html': html,
    'application/json': json
  })

  req.negotiate(negotiation)

}

router.head('*', (req, res, next) => {
  res.header('Link', '</webmentions/hook>; rel="webmention"')
  res.status(200).send()
})

router.get('*', (req, res, next) => {

  console.log(req.url)

  res.locals.loggedin = req.isAuthenticated()
  res.locals.env = process.env.NODE_ENV
  res.locals.production = process.env.NODE_ENV === 'production'
  res.locals.vapidPublicKey = process.env.VAPID_PUBLIC_KEY

  const actions = [
    moduleMeta.then(meta => {
      res.locals.moduleGraph = req.isAuthenticated() ? meta.fullGraph : meta.graph
      res.locals.moduleHashes = req.isAuthenticated() ? meta.fullHashes : meta.hashes
    })
  ]

  if (req.isAuthenticated()) {

    if (req.query.action === 'delete') {
      actions.push(ComponentService.remove(req.query.type, {id: req.query.id}))
    }

  }

  Promise.all(actions)
    .then(() => next())
    .catch(next)

})

router.get('/:slug', maybeLogin(
  (req, res, next) => {

    ComponentService.getTouchedPopulated('Article', {slug: req.params.slug})
      .then(response => send(req, res, Object.assign(response, {
        template: 'templates/article',
        feature: ['dpdk', 'clever-franke', 'studio-naam', 'kaliber', 'born-05'].indexOf(req.params.slug) !== -1,
        pageTitle: response.title
      })))
      .catch(next)

  },

  (req, res, next) => {

    ComponentService.getPopulated('Article', {slug: req.params.slug})
      .then(response => send(req, res, Object.assign(response, {
        template: 'templates/article',
        feature: ['dpdk', 'clever-franke', 'studio-naam', 'kaliber', 'born-05'].indexOf(req.params.slug) !== -1,
        pageTitle: response.title
      })))
      .catch(next)

  }
))

router.get('/', (req, res, next) => {

  const options = req.isAuthenticated() ? {} : {published: 'yes'}
  const data = [
    ComponentService.getAll('Article', options),
    TwitterService.getAll({})
  ]

  Promise.all(data)
    .then(([articles, tweets]) => send(req, res, {
      articles: articles.reverse(),
      pageTitle: articles.length > 1 ? 'My articles' : 'My first article:',
      tweets: tweets.slice(0, articles.length + 1),
      template: 'templates/index',
      isHome: true
    }))
    .catch(next)

})

module.exports = router
