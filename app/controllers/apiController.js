const router = require('express').Router()
const JsonAction = require('actions/JsonAction')
const HtmlAction = require('actions/HtmlAction')
const ComponentService = require('services/ComponentService')
const SubscriptionService = require('services/SubscriptionService')

// When not on production you can use the API through the browser address bar
const action = process.env.NODE_ENV === 'production' ? 'post' : 'use'

router[action]('/update/:type/:id', (req, res, next) => {

  console.log('update', req.params.id, 'with', req.body)

  ComponentService.update(req.params.type, {_id: req.params.id}, req.body)
    .then(() => {

      const action = new JsonAction(req, res)
      action.render({
        status: 200,
        message: req.params.id + ' was updated.'
      })

    })
    .catch(next)

})

router[action]('/delete/:type/:id', (req, res, next) => {

  ComponentService.remove(req.params.type, {_id: req.params.id})
    .then(() => {

      const action = new JsonAction(req, res)
      action.render({status: 200, message: req.params.id + ' was removed.'})

    })
    .catch(next)

})

router[action]('/create/:type/:discriminator', (req, res, next) => {

  ComponentService.createDiscriminator(req.params.type, req.params.discriminator, req.body || {})
    .then(component => {

      const action = new JsonAction(req, res)

      action.render({
        status: 200,
        message: 'component created.',
        component
      })

    })
    .catch(next)

})

router[action]('/create/:type/', (req, res, next) => {

  ComponentService.create(req.params.type, req.body || {})
    .then(component => {

      const action = new JsonAction(req, res)

      action.render({
        status: 200,
        message: 'component created.',
        component
      })

    })
    .catch(next)

})

router.get('/render/partial/:partial(*)', (req, res, next) => {
  console.log(req.query)
  res.render(req.params.partial, req.query, (err, html) => {

    if (err) next(err)

    const action = new JsonAction(req, res)

    action.render({
      status: 200,
      message: 'component rendered.',
      html
    })

  })

})

router[action]('/subscription/create/', (req, res, next) => {

  console.log('create new sub', req.body)

  SubscriptionService.create(req.body)
    .then(() => new JsonAction(req, res))
    .then(action => action.render({
      status: 200,
      message: 'succesfully subscribed.'
    }))
    .catch(next)

})

router.get('/push/article/:id', (req, res, next) => {

  const query = {_id: req.params.id}

  const validate = response => {

    if (!response.published)
      return Promise.reject(new Error('Article is not published'))

    if (response.pushed)
      return Promise.reject(new Error('Article was already pushed'))

    return response;

  }

  const render = () => {
    const action = new JsonAction(req, res)
    action.render({status: 200, message: 'Message published.'})
  }

  ComponentService.get('Article', query)
    .then(validate)
    .then(response => SubscriptionService.publish({
      title: 'New: ' + response.title,
      url: 'https://peeke.nl/' + response.slug
    }))
    .then(render)
    .then(() => ComponentService.update('article', query, { pushed: true }))
    .catch(next)

})

module.exports = router
