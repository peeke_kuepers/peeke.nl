const router = require('express').Router()
const WebmentionService = require('services/WebmentionService');
const HtmlAction = require('actions/HtmlAction')

const send = (req, res, data) => {

  Object.assign(data, {layout: 'layouts/page'})

  const html = () => (new HtmlAction(req, res)).render(data)
  const json = () => (new JsonAction(req, res)).render(data)

  const negotiation = {'default': html}

  req.isAuthenticated() || Object.assign(negotiation, {
    'text/html': html,
    'application/json': json
  })

  req.negotiate(negotiation)

}

router.get('/:url', (req, res, next) => {

  WebmentionService.get(req.params.url)
    .then(mentions => send(req, res, {
      mentions,
      url: req.params.url,
      pageTitle: `Web Mentions for ${ req.params.url }`,
      template: 'templates/mentions'
    }))
    .catch(next)

})

module.exports = router