const router = require('express').Router()
const ComponentService = require('services/ComponentService')
const RSS = require('rss');

router.get('*', (req, res, next) => {

  ComponentService.getAll('Article', {published: 'yes'})
    .then(articles => buildFeed(articles.reverse()))
    .then(xml => {
      res.set('Content-Type', 'text/xml');
      res.send(xml)
    })
    .catch(next)
})

const buildFeed = articles => {

  /* lets create an rss feed */
  const feed = new RSS({
    title: 'Peeke.nl',
    description: 'I am not a designer, nor a developer. I\'m a bit of both. Frontender at @_Mirabeau',
    feed_url: 'https://peeke.nl/rss',
    site_url: 'https://peeke.nl',
    image_url: 'https://peeke.nl/favicon-128.png',
    copyright: 'Peeke Kuepers',
    language: 'en',
    ttl: '60'
  });

  articles.forEach(article => {
    /* loop over data and add to feed */
    feed.item({
      title:  article.title,
      description: article.subtitle,
      url: 'https://peeke.nl/' + article.slug, // link to the item
      author: 'Peeke Kuepers', // optional - defaults to feed author property
      date: article.datetime, // any format that js Date can parse.
    });
  })

  // cache the xml to send to clients
  return feed.xml();

}

module.exports = router
