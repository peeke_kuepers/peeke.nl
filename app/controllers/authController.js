const router = require('express').Router()
const passport = require('passport')
const authenticate = passport.authenticate('auth0', {
  failureRedirect: '/auth/login'
})

const HtmlAction = require('actions/HtmlAction')

router.get('/handle', authenticate, (req, res, next) => {

  if (!req.user) {
    next(new Error('user null'))
  }

  res.redirect('/')

})

router.get('/login', (req, res) => {

  if (req.isAuthenticated()) {
    return res.redirect('/')
  }

  const action = new HtmlAction(req, res, 'layouts/page', 'templates/login')
  action.render()

})

router.get('/logout', (req, res) => {
  req.logout()
  res.redirect('/')
})

module.exports = router