const router = require('express').Router()
const TwitterService = require('services/TwitterService.js')

//const HtmlAction = require('actions/HtmlAction');
const JsonAction = require('actions/JsonAction')

router.get('/tweets', (req, res, next) => {

  if (!req.isAuthenticated()) {
    next()
  }

  TwitterService.get()
    .then(latestTweet => TwitterService.timeline(latestTweet ? latestTweet.tweetId : null))
    .then(tweets => tweets.map(tweet => TwitterService.create(tweet)))
    .then(() => res.send('OK'))
    .catch(next)

})

module.exports = router