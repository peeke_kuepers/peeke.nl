'use strict'

// Support for ServiceWorkers and ES6 arrow functions nicely align, so we'll use them :)

const CACHE_NAME = 'RESOURCE_CACHE_V1'
const IMMUTABLE = [
  /\?hash=[0-9a-z]+$/, // hashed resources
  /^(https|http):\/\/fonts\.gstatic\.com/,
  /^(https|http):\/\/cdn/
]

const cacheResponse = (request, response) => {

  // Cloning is necessary, because the response body is consumed in the process and therefore can't be used again
  return caches.open(CACHE_NAME)
    .then(cache => cache.put(request, response.clone()))
    .then(() => response)

}

const getResponseFromCache = request => {
  return caches.open(CACHE_NAME)
    .then(cache => cache.match(request))
    .then(response => response ? response : Promise.reject())
}

const clearCaches = () => {
  const clear = keys => Promise.all(keys.map(key => caches.delete(key)))
  return caches.keys().then(clear)
}

const cacheFirst = request => {
  return getResponseFromCache(request)
    .catch(() => fetch(request))
    .then(response => cacheResponse(request, response))
}

const networkFirst = request => {
  return fetch(request)
    .then(response => cacheResponse(request, response))
    .catch(() => getResponseFromCache(request))
}

self.addEventListener('install', () => {
  console.log('Installing new service worker...')
})

self.addEventListener('activate', event => {
  event.waitUntil(clearCaches())
  console.log('Running')
})

self.addEventListener('push', event => {

  const payload = event.data.json()
  const options = {
    body: payload.body,
    data: {
      url: payload.url
    }
  }

  event.waitUntil(self.registration.showNotification(payload.title, options))

})

self.addEventListener('notificationclick', event => {

  const openedWindow = clients.matchAll({type: 'window'})
    .then(() => clients.openWindow(event.notification.data.url))

  event.notification.close()
  event.waitUntil(openedWindow)

})

self.addEventListener('fetch', event => {

  if (event.request.method !== 'GET') {
    return event.respondWith(fetch(event.request))
  }

  const isImmutable = IMMUTABLE.filter(pat => pat.test(event.request.url)).length > 0
  const response = isImmutable
    ? cacheFirst(event.request)
    : networkFirst(event.request)

  event.respondWith(response)

})