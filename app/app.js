process.env.NODE_PATH = __dirname
require('module').Module._initPaths()
require('dotenv').config()

const express = require('express')
const negotiate = require('express-negotiate')
const engine = require('./engine')
const path = require('path')
const favicon = require('serve-favicon')
const morgan = require('morgan')
const logger = morgan('combined', {
  skip: function (req, res) { return res.statusCode < 400 }
})
const passport = require('passport')
const strategy = require('helpers/Auth0PassportStrategy')
const session = require('express-session')
const cookieParser = require('cookie-parser')
const bodyParser = require('body-parser')

const apiController = require('controllers/apiController')
const authController = require('controllers/authController')
const indexController = require('controllers/indexController')
const importController = require('controllers/importController')
const webmentionController = require('controllers/webmentionController')
const rssController = require('controllers/rssController')

const ErrorAction = require('actions/ErrorAction')

const app = express()

// view engine setup
engine.localsAsTemplateData(app)
app.engine('hbs', engine.__express)
app.set('view engine', 'hbs')
app.set('views', __dirname + '/views')

// uncomment after placing your favicon in /public
// app.use(favicon(path.join(__dirname, '../dist/static', 'favicon.ico')));
app.use(logger)
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({
  extended: false
}))
app.use(cookieParser())
app.use(session({
  secret: process.env.AUTH0_CLIENT_SECRET,
  resave: false,
  saveUninitialized: false
}))
app.use(passport.initialize())
app.use(passport.session())
app.use(express.static(path.join(__dirname, '../dist/static'), { maxAge: 7776000 }))
app.use(express.static(path.join(__dirname, '../public'), { maxAge: 86400 }))

app.use('/sw.js', (req, res) => res.sendFile(path.join(__dirname, 'sw.js')))
app.use('/api', apiController)
app.use('/auth', authController)
app.use('/import', importController)
app.use('/webmentions', webmentionController)
app.use('/rss', rssController)
app.use('/', indexController)

function message404 () {

  const messages = [
    'Nope, doesn\'t ring a bell',
    'Sorry, we lost this page',
    'Aka: we couldn\'t find this page',
    'Went missing: url',
    'This page left the club',
    'You took the wrong door',
    'Nothing to see here, just walk along',
    'Lost. Return for finders fee',
    'Are you refreshing a 404 page?'
  ]

  return messages[Math.floor(Math.random() * messages.length)]

}

// catch 404 and forward to error handler
app.use((req, res, next) => {
  console.log(message404())
  var err = new Error(message404())
  err.status = 404
  next(err)
})

// error handlers

app.use((err, req, res, next) => {
  const action = new ErrorAction(req, res, err)
  action.render()
})

console.log(`Running in '${ process.env.NODE_ENV }' mode`)

module.exports = app
