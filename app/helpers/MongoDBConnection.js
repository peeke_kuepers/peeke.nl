const odm = require('mongoose')

odm.Promise = global.Promise

const ready = new Promise((resolve, reject) => {

  odm.connect(process.env.DATABASE || 'mongodb://localhost:27017/peekenl-development')

  const db = odm.connection

  db.on('error', reject)
  db.once('open', resolve)

})

module.exports = {ready}