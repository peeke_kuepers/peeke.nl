const odm = require('mongoose')

module.exports = function (modelName, schema) {
  schema.virtual('type').get(function () { return modelName })
  schema.virtual('id').get(function () { return this._id })
  return odm.model(modelName, schema)
}