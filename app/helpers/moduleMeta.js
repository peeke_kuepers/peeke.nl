const madge = require('madge')
const glob = require('glob')
const crypto = require('crypto')
const fs = require('fs')

const checksum = data => crypto.createHash('md5').update(data, 'utf8').digest('hex')
const hash = path => checksum(fs.readFileSync(path)).slice(0, 10)

const graph = new Promise(resolve => {

  glob('./src/static/js/+(ui|visuals)/**/*.js', (err, files) => {
    madge(files).then(res => resolve(res.obj()))
  })

})

const fullGraph = new Promise(resolve => {

  glob('./src/static/js/**/*.js', (err, files) => {
    madge(files).then(res => resolve(res.obj()))
  })

})

const resolveHashes = graph => new Promise(resolve => {

  graph.then(graph => {
    const hashes_ = Object
      .keys(graph)
      .map(path => ({
        path,
        hash: hash(`./src/static/js/${ path }.js`)
      }))
    resolve(hashes_)
  })

})

const hashes = resolveHashes(graph)
const fullHashes = resolveHashes(fullGraph)

const meta = Promise.all([graph, fullGraph, hashes, fullHashes])
  .then(([graph, fullGraph, hashes, fullHashes]) => ({graph, fullGraph, hashes, fullHashes}))

module.exports = meta
