module.exports = {

  requireLogin: (req, res, next) => {
    if (!req.isAuthenticated()) {
      return res.redirect('/auth/handle')
    }
    next()
  },

  maybeLogin: (loggedIn, loggedOut) => (req, res, next) => {

    if (req.isAuthenticated()) {
      loggedIn(req, res, next)
    } else {
      loggedOut(req, res, next)
    }

  },

}