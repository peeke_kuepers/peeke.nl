const crypto = require('crypto')
const fs = require('fs')

const checksum = data => crypto.createHash('md5').update(data, 'utf8').digest('hex')
const hash = path => checksum(fs.readFileSync(path)).slice(0, 10)

module.exports = (base) => {
  return {
    path: path => path + '?hash=' + hash(base + path),
    hash: path => hash(base + path)
  }
}