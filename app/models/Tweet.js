const odm = require('mongoose')
const model = require('helpers/mongooseBootstrapModel')

const schema = new odm.Schema({

  retweet: Boolean,
  authorHandle: String,
  authorName: String,
  body: String,
  tweetId: String,
  date: Number

})

module.exports = model('Tweet', schema)