const odm = require('mongoose')
const model = require('helpers/mongooseBootstrapModel')

const schema = new odm.Schema({
  components: [{type: odm.Schema.Types.ObjectId, ref: 'Component'}]
}, {
  discriminatorKey: 'discriminator',
  toObject: {virtuals: true},
  toJSON: {virtuals: true}
})

const populate = function (next) {
  this.populate('components')
  next()
}

schema
  .pre('findOne', populate)
  .pre('find', populate)

module.exports = model('Component', schema)