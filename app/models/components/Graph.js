const odm = require('mongoose')
const Component = require('models/Component')
const unique = arr => [...new Set(...arr)]

const schema = new odm.Schema({
  domain: String,
  domainLabel: String,
  range: String,
  rangeLabel: String,
  fns: {
    type: String,
    default: ''
  },
  resolution: Number,
  animated: Boolean,
  legend: {
    type: Boolean,
    default: true
  },
  lowlight: String,
  ruler: String,
  colors: String,
  allowedComponents: {
    type: Array,
    default: [ 'Input' ]
  },
  components: [{type: odm.Schema.Types.ObjectId, ref: 'Component'}]
}, {
  discriminatorKey: 'discriminator'
})

schema.virtual('inputs').get(function () {
  return this.components.filter(component => component.discriminator === 'Input')
})

schema.virtual('inputs').set(function (ids) {
  this.components = unique(this.components.concat(ids))
})

module.exports = Component.discriminator('Graph', schema)