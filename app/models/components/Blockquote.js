const odm = require('mongoose')
const Component = require('models/Component')

const schema = new odm.Schema({
  text: String,
  source: String
}, {discriminatorKey: 'discriminator'})

schema.virtual('size').get(function () {
  if (!this.text) return 'small'
  return this.text.length > 110 ? 'large' : 'small'
})

module.exports = Component.discriminator('Blockquote', schema)