const odm = require('mongoose')
const Component = require('models/Component')
const marked = require('marked')
marked.setOptions({
  gfm: true,
  breaks: true
})

const schema = new odm.Schema({
  _markdown: String,
  body: String
}, {discriminatorKey: 'discriminator'})

schema.virtual('markdown').get(function () {
  return this._markdown
})

schema.virtual('editableMarkdown').get(function () {
  return this._markdown ? this._markdown.replace(/(?:\r\n|\r|\n)/g, '<br>') : ''
})

schema.virtual('markdown').set(function (markdown) {
  this._markdown = markdown
  this.body = marked(markdown)
})

module.exports = Component.discriminator('Markdown', schema)