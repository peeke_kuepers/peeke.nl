const odm = require('mongoose')
const Component = require('models/Component')
const Prism = require('prismjs')
const highlight = c => Prism.highlight(c, Prism.languages.javascript)
const decodeEntities = new (require('html-entities').AllHtmlEntities)().decode

const schema = new odm.Schema({
  _code: String,
  body: String
}, {discriminatorKey: 'discriminator'})

schema.virtual('code').get(function () {
  return this._code
})

schema.virtual('editableCode').get(function () {
  return this._code ? this._code.replace(/(?:\r\n|\r|\n)/g, '<br>') : ''
})

schema.virtual('code').set(function (code) {
  this._code = code
  this.body = highlight(decodeEntities(code)).replace(/(?:\r\n|\r|\n)/g, '<br>')
})

module.exports = Component.discriminator('Code', schema)