const odm = require('mongoose')
const Component = require('models/Component')

const schema = new odm.Schema({
  name: String,
  value: String,
  attributes: String,
  inputType: String
}, {discriminatorKey: 'discriminator'})

module.exports = Component.discriminator('Input', schema)