const odm = require('mongoose')
const model = require('helpers/mongooseBootstrapModel')
require('models/Component')

const schema = new odm.Schema({
  title: String,
  subtitle: String,
  intro: String,
  published: {
    type: String,
    default: ''
  },
  pushed: {
    type: Boolean,
    default: false
  },
  slug: {
    type: String,
    unique: true,
    required: true,
    validate: v => /^[A-Za-z0-9]+(?:-[A-Za-z0-9]+)*$/.test(v)
  },
  syndicationLinks: {
    type: String,
    default: ''
  },
  systemDate: {
    type: Date,
    default: Date.now
  },
  visual: {
    type: String,
    validate: v => /[A-Za-z0-9\/]/.test(v)
  },
  allowedComponents: {
    type: Array,
    default: ['Blockquote', 'Graph', 'Markdown', 'Code']
  },
  components: [{type: odm.Schema.Types.ObjectId, ref: 'Component'}]
}, {
  toObject: {virtuals: true},
  toJSON: {virtuals: true}
})

const populate = function (next) {
  this.populate('components')
  next()
}

schema
  .pre('findOne', populate)
  .pre('find', populate)

schema.virtual('date').get(function () {
  const d = new Date(this.systemDate)
  return `${ d.getDate() }-${ d.getMonth() + 1 }-${ d.getFullYear() } ${ d.getHours() }:${ d.getMinutes() }`
})

schema.virtual('datetime').get(function () {
  const d = new Date(this.systemDate)
  return `${ d.getFullYear() }-${ d.getMonth() + 1 }-${ d.getDate() } ${ d.getHours() }:${ d.getMinutes() }`
})

schema.virtual('date').set(function (dateString) {

  const dateSegs = dateString.split(' ')[0].split(/[\s\\\/-]+/)
  const time = dateString.split(' ')[1];

  [dateSegs[0], dateSegs[1]] = [dateSegs[1], dateSegs[0]]

  this.systemDate = new Date([dateSegs.join('/'), time].join(' '))

})

schema.virtual('mainSyndicationLink').get(function () {
  this.syndicationLinks.split(',').pop()
})

module.exports = model('Article', schema)