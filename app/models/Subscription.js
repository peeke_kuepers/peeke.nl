const odm = require('mongoose')
const model = require('helpers/mongooseBootstrapModel')

const schema = new odm.Schema({

  endpoint: {
    type: String,
    required: true
  },
  keys: {
    type: Object,
    required: true
  },
  date: {
    type: Date,
    default: Date.now
  }

})

module.exports = model('Subscription', schema)