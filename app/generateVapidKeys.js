const webpush = require('web-push')
const vapidKeys = webpush.generateVAPIDKeys()
const urlsafeBase64 = require('urlsafe-base64')

console.log(`VAPID_PUBLIC_KEY=${ urlsafeBase64.encode(vapidKeys.publicKey) }`)
console.log(`VAPID_PRIVATE_KEY=${ urlsafeBase64.encode(vapidKeys.privateKey) }`)