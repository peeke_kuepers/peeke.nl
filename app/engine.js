const hbs = require('hbs')
const engine = hbs.create()
const path = require('path')
const hasher = require('helpers/hbsHashHelper')(path.join(__dirname, '../dist/static'))

const templateName = component => {
  if (!component.type) throw new Error('Trying to resolve non-existing item ' + component)
  return component.discriminator
    ? component.discriminator.toLowerCase()
    : component.type.toLowerCase()
}

engine.registerHelper('resolve', (component, stuff) => [stuff, templateName(component)].join('/'))
engine.registerHelper('refs', value => {
  if (!value) return '';
  return value.length && typeof value[0] === 'object'
    ? value.map(o => o._id).join(',')
    : value.join(',')
})
engine.registerHelper('sanitize', v => v.replace(/\W+/g, '-'))
engine.registerHelper('join', a => a.join(','))
engine.registerHelper('hashedpath', hasher.path)
engine.registerHelper('hash', hasher.hash)
engine.registerHelper('json', value => JSON.stringify(value))
engine.registerHelper('each', (context, options) => {
  if (typeof context === 'string') context = context.split(',').map(v => v.trim())
  return (context || []).reduce((r, item) => r + options.fn(item), '')
})

engine.registerPartials(__dirname + '/views')

module.exports = engine
