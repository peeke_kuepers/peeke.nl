const connectionReady = require('helpers/MongoDBConnection').ready
const models = require('models')
const debug = r => {
  console.log('debug', r)
  return r
}

function pascalCase (string) {
  return string[0].toUpperCase() + string.slice(1)
}

function model (type) {
  return models[pascalCase(type)]
}

class MongoDBRepository {

  constructor () {}

  // Write

  static create (type, doc) {
    return models[pascalCase(type)]
      .create(doc)
  }

  static createDiscriminator (type, discriminator, doc) {
    return models[type.toLowerCase() + 's'][pascalCase(discriminator)]
      .create(doc)
  }

  // Read

  static findOne (type, query = {}, sort = '') {

    return connectionReady
      .then(() => model(type).findOne(query).sort(sort))
  }

  static find (type, query = {}, sort = '') {
    return connectionReady
      .then(() => model(type).find(query).sort(sort))
  }

  // Update

  static update (type, query, updates) {
    return connectionReady
      .then(() => model(type).find(query))
      .then(docs => docs.map(doc => Object.assign(doc, updates)))
      .then(docs => Promise.all(docs.map(doc => doc.save())))
  }

  // Delete

  static remove (type, query) {
    return connectionReady
      .then(() => model(type).remove(query).exec())
  }

}

module.exports = MongoDBRepository
