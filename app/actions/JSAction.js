class JsAction {

  constructor (req, res) {
    this._res = res
  }

  render (body = '') {

    this._res.contentType('application/javascript')
    this._res.send(body)

  }
}

module.exports = JsAction
