class HtmlAction {

  constructor (req, res, layout = 'layouts/page', template = 'templates/index') {

    this._req = req
    this._res = res

    this._res.locals.layout = layout
    this._res.locals.template = template

  }

  render (data = {}) {

    const {template} = this._res.locals

    this._res.render(template, data)

  }
}

module.exports = HtmlAction
