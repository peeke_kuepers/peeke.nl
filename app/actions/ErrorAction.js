class ErrorAction {

  constructor (req, res, err) {

    this._error = err
    this._res = res
    this._req = req

    console.error(err)

  }

  render () {

    let status = this._error.status || 500,
      data = {
        status,
        title: 'Error: ' + status,
        message: this._error.message,
        template: 'templates/error',
        layout: 'layouts/page'
      }

    if (this._error.trace && process.env.NODE_ENV !== 'production') {
      data.stack = this._error.stack
    }

    this._res.status(status)

    this._res.render(data.layout, data)

    console.log('Error at ', this._req.originalUrl)

  }

}

module.exports = ErrorAction