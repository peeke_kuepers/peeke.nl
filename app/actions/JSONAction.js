class JsonAction {

  constructor (req, res) {
    this._req = req
    this._res = res
  }

  render (data = {}) {
    this._res.header('Render-Method', 'client')
    this._res.contentType('application/json')
    this._res.status(200)
    this._res.send(JSON.stringify(data))
  }

}

module.exports = JsonAction
