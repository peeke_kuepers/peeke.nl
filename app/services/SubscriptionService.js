const webpush = require('web-push')
const MongoDBRepository = require('repositories/MongoDBRepository')

webpush.setVapidDetails(
  'mailto:info@peeke.nl',
  process.env.VAPID_PUBLIC_KEY,
  process.env.VAPID_PRIVATE_KEY
)

class SubscriptionService {

  static create (subscription) {
    return MongoDBRepository.create('Subscription', subscription)
  }

  static remove (query) {
    return MongoDBRepository.remove('Subscription', query)
  }

  static find (query) {
    return MongoDBRepository.find('Subscription', query)
  }

  static publish (publication) {

    const push = subscription =>
      SubscriptionService.push(subscription, JSON.stringify(publication))

    return MongoDBRepository.find('Subscription')
      .then(subscriptions => subscriptions.map(push))
      .then(pushes => Promise.all(pushes))

  }

  static push(subscription, payload) {
    return webpush.sendNotification(subscription, payload)
      .catch(() => SubscriptionService.remove({ _id: subscription._id }))
  }

}

module.exports = SubscriptionService