const MongoDBRepository = require('repositories/MongoDBRepository')
const debug = r => {
  console.log(r)
  return r
}

class ComponentService {

  constructor () {}

  static touch (type, query) {

    return MongoDBRepository.findOne(type, query)
      .then(doc => doc || MongoDBRepository.create(type, query))

  }

  static get (type, query) {
    return MongoDBRepository.findOne(type, query)
  }

  static getAll (type, query) {
    return MongoDBRepository.find(type, query)
  }

  static getPopulated (type, query) {
    return MongoDBRepository.findOne(type, query)
  }

  static getTouchedPopulated (type, query) {
    return ComponentService.touch(type, query)
      .then(() => MongoDBRepository.findOne(type, query))
  }

  static create (type, setup) {
    return MongoDBRepository.create(type, setup)
  }

  static createDiscriminator (type, discriminator, setup) {
    return MongoDBRepository.createDiscriminator(type, discriminator, setup)
  }

  static update (type, query, updates) {
    return MongoDBRepository.update(type, query, updates)
  }

  static remove (type, query) {
    return MongoDBRepository.remove(type, query)
  }

}

module.exports = ComponentService
