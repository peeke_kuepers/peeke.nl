const MongoDBRepository = require('repositories/MongoDBRepository')
const Twitter = require('twitter')
const client = new Twitter({
  consumer_key: process.env.TWITTER_CONSUMER_KEY,
  consumer_secret: process.env.TWITTER_CONSUMER_SECRET,
  access_token_key: process.env.TWITTER_TOKEN,
  access_token_secret: process.env.TWITTER_TOKEN_SECRET
})

const mapTweet = tweet => {

  const tweetId = tweet.id_str
  const date = new Date(tweet.created_at).getTime()
  const retweet = tweet.retweeted
  const authorHandle = retweet ? tweet.entities.user_mentions[0].screen_name : 'peeke91'
  const authorName = retweet ? tweet.entities.user_mentions[0].name : 'Me'

  const entities = retweet ? tweet.retweeted_status.entities : tweet.entities

  const text = retweet ? tweet.retweeted_status.text : tweet.text
  let body = text

  entities.hashtags.forEach(hashtag => {
    const replace = text.substring(hashtag.indices[0], hashtag.indices[1])
    const string = `<a href="https://twitter.com/hashtag/${ hashtag.text }">#${ hashtag.text }</a>`
    body = body.replace(new RegExp(replace, 'g'), string)
  })

  entities.user_mentions.forEach(user_mention => {
    const replace = text.substring(user_mention.indices[0], user_mention.indices[1])
    const string = `<a href="https://twitter.com/${ user_mention.text }">@${ user_mention.screen_name }</a>`
    body = body.replace(new RegExp(replace, 'g'), string)
  })

  entities.urls.forEach(url => {
    const replace = text.substring(url.indices[0], url.indices[1])
    const string = `<a href="${ url.expanded_url }">${ url.display_url }</a>`
    body = body.replace(new RegExp(replace, 'g'), string)
  })

  return {
    retweet,
    authorHandle,
    authorName,
    body,
    tweetId,
    date
  }

}

class TwitterService {

  constructor () {}

  static timeline (sinceId, count = 10) {

    const options = {
      count,
      trim_user: true,
      exclude_replies: true
    }

    sinceId && Object.assign(options, {since_id: sinceId})

    return new Promise((resolve, reject) => {

      client.get('statuses/user_timeline', options, (error, tweets, response) => {

        if (error) {
          return reject(error)
        }

        tweets = tweets
          .filter(tweet => tweet.lang !== 'nl')
          .map(mapTweet)

        resolve(tweets)

      })

    })

  }

  static create (tweet) {
    return MongoDBRepository.create('Tweet', tweet)
  }

  static delete (tweet) {
    return MongoDBRepository.findAndDelete('Tweet', tweet)
  }

  static get (query) {
    return MongoDBRepository.find('Tweet', query, '-tweetId')
  }

  static getAll (query) {
    return MongoDBRepository.find('Tweet', query, '-tweetId')
  }

}

module.exports = TwitterService