const fetch = require('node-fetch')

const webmentions = url => fetch(`https://webmention.io/api/mentions.jf2?token=${ process.env.WEBMENTION_IO_TOKEN }&per-page=1000&target=${ url }`)

const type = mention => {
  const types = {
    'in-reply-to': 'comment',
    'mention-of': 'comment',
    'like-of': 'like',
    'repost-of': 'retweet'
  }
  return types[mention['wm-property']]
}

const isComment = mention => type(mention) === 'comment'
const isLike = mention => type(mention) === 'like'
const isRetweet = mention => type(mention) === 'retweet'

const mapMention = mention => ({
  author: mention.author,
  content: mention.content && mention.content.value,
  url: mention.url
})

class WebmentionService {

  static get(url) {

    return webmentions(url)
      .then(response => response.json())
      .then(response => response.children)
      .then(mentions => {

        const likes = mentions.filter(isLike).map(mapMention)
        const retweets = mentions.filter(isRetweet).map(mapMention)
        const total = likes.length + retweets.length;

        return {
          comments: mentions.filter(isComment).map(mapMention),
          likes: {
            count: likes.length,
            first: likes.slice(0, Math.round(20 / total * likes.length))
          },
          retweets: {
            count: retweets.length,
            first: retweets.slice(0, Math.round(20 / total * retweets.length))
          }
        }
      })

  }

}

module.exports = WebmentionService