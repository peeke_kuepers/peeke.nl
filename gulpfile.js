var gulp 			= require('gulp');
var changed			= require('gulp-changed');
var sass 			= require('gulp-sass');
var postcss      	= require('gulp-postcss');
var autoprefixer 	= require('autoprefixer');
var minifyCss 		= require('gulp-minify-css');
var uglifyJs		= require('uglify-js-harmony');
var minifyJs		= require('gulp-uglify/minifier');
var babel			= require('gulp-babel');
var sourcemaps 		= require('gulp-sourcemaps');
var gulpif 			= require('gulp-if');
var transform		= require('gulp-transform');
var fs 				= require('fs');
var browserify		= require('browserify');

var config = {
	paths: {
		scss: 'scss',
		css: 'css',
		media: 'media',
		components: 'components',
		views: 'views',
		js: 'js'
	}
};

var src = './src/static/';
var app = './app/';
var dist = './dist/static/';
var shared = './shared/';
var all = '/**/*';
var children = '/*';
var paths = config.paths;

function ext(ext) { return '.' + (Array.isArray(ext) ? '{' + ext.join(',') + '}' : ext); }

gulp.task('sass:compile', function() {
	gulp.src(src + paths.scss + all + ext('scss'))
		.pipe(sass().on('error', console.log))
		.pipe(postcss([ autoprefixer({ browsers: ['last 2 versions'] }) ]))
		.pipe(minifyCss())
		.pipe(gulp.dest(dist + paths.css));
});

gulp.task('sass:watch', ['sass:compile'], function () {
	gulp.watch(src + paths.scss + all + ext('scss'), ['sass:compile']);
});

gulp.task('js:compile', function() {

	const dest = dist + paths.js;

	function transpile(file) {
		return !/es5/.test(file.path) && !/sw/.test(file.path);
	}

	gulp.src(src + paths.js + all + ext('js'))
		.pipe(changed(dest))
		.pipe(sourcemaps.init())
		.pipe(gulpif(transpile, babel()))
		.pipe(minifyJs({}, uglifyJs))
		.on('error', console.log)
		.pipe(sourcemaps.write('./'))
		.pipe(gulp.dest(dest));

});

gulp.task('js:watch', ['js:compile'], function () {
	gulp.watch(src + paths.js + all + ext('js'), ['js:compile']);
});

var misc = [src + paths.media + all];
gulp.task('misc:copy', function(){
	gulp.src(misc).pipe(gulp.dest(dist + paths.media));
});

gulp.task('misc:watch', ['misc:copy'], function(){
	gulp.watch(misc, ['misc:copy']);
});

var components = [app + paths.views + '/' + paths.components + all + ext('hbs')];
gulp.task('components:decmsify', function() {
	const dest = app + paths.views + '/client-' + paths.components;
	return gulp.src(components)
		.pipe(changed(dest))
		.pipe(transform(decmsify, {encoding: 'utf8'}))
		.pipe(gulp.dest(dest));
});

gulp.task('components:watch', function() {
	return gulp.watch(components, ['components:decmsify']);
});

gulp.task('watch', ['sass:watch', 'misc:watch', 'js:watch', 'components:watch']);

gulp.task('dev', ['watch']);

function decmsify(input) {
    const regex = /<!--\s*client: (.*?) -->/g;
    let match;
    input = input.replace(/<!-- cms-only -->[\s\S]*?<!-- !cms-only -->/g, '');
    input = input.replace(/ data-cms[\s\S]*?("|')[\s\S]*?("|')/g, '');
    while ((match = regex.exec(input)) !== null) {
    	const repl = match[1].split(' -> ');
        input = input.replace(new RegExp(match[0], 'g'), '')
        input = input.replace(new RegExp(repl[0], 'g'), repl[1])
    }
	return input;
}