import loader from 'loader';

class Reload {

    constructor(element) {

        const selector = element.dataset.area;

        element.addEventListener('click', e => {

            const area = document.querySelector(selector);

            fetch(window.location.href)
                .then(response => response.text())
                .then(html => {

                    const parser = new DOMParser();
                    const result = parser.parseFromString(html, 'text/html');
                    const newArea = result.querySelector(selector);

                    console.log(newArea);

                    area.parentNode.replaceChild(newArea, area);

                    loader.parse(newArea)
                        .then(console.log)
                        .catch(console.log);

                });

        });

    }

}

export default Reload;
