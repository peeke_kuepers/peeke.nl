const constants = {
	COLLECTION_ATTR: 'data-cms-collection',
	ALLOWED_TYPES_ATTR: 'data-cms-allowed-types',
	PROPERTY_ATTR: 'data-cms-property',
	VALUE_ATTR: 'data-cms-value',
	ID_ATTR: 'data-cms-id',
	TYPE_ATTR: 'data-cms-type',
	CMS_ENABLED_ATTR: 'data-cms-enabled',
	DRAGGING_ATTR: 'data-cms-dragging',
	DROPAREA_ATTR: 'data-cms-droparea',
	DELETABLE_ATTR: 'data-cms-deletable',
	ALLOW_BREAKS_ATTR: 'data-cms-allow-breaks',
	ALLOW_TABS_ATTR: 'data-cms-allow-tabs',
	EDITABLE_ELEMENT: 'data-editable-element',
	KEY: {
		SHIFT: 16,
        ENTER: 13,
		BACKSPACE: 8,
		TAB: 9
	}
};

export default constants;
