import C from 'cms/Constants';
import { debounceTail, dispatchEvent } from 'util/eventHelpers';

const CHAR_TAB = '	';
const CHAR_4NBSP = '&nbsp;&nbsp;&nbsp;&nbsp;';

class Editable {

	constructor(element, options) {

		this._options = Object.assign(Editable.options, options);
		this._elements = {
			self: element,
			editable: this._options.editable || element
        };

		this._onKeyDown = this._onKeyDown.bind(this);
		this._handlePaste = this._handlePaste.bind(this);
        this._onChange = this._onChange.bind(this);
        this._onKeyUp = this._onKeyUp.bind(this);

		this.editable = true;
        this.interactive = true;

	}

	static get options() {
		return {
			allowBreaks: false,
			allowTabs: false,
			deletable: true,
			editable: null
		}
	}

    get value() {

		const value = this._options.allowBreaks
			? this._elements.editable.innerHTML
			: this._elements.editable.textContent;

        return value.replace(/^ +/gm, '').replace(/<br>/g, '\n');

    }

    set value(value) {
		if (!this.editable) return;
        this._elements.editable[this._options.allowBreaks ? 'innerHTML' : 'textContent' ] = value;
	}

    set editable(bool) {
        this._elements.editable.setAttribute('contenteditable', bool);
    }

    get editable() {
        return this._elements.editable.getAttribute('contenteditable') === 'true';
    }

    set interactive(bool) {

        if (this.interactive === bool) return;

        const action = bool ? 'addEventListener' : 'removeEventListener';

        this._elements.editable[action]('keydown', this._onKeyDown);
        this._elements.editable[action]('paste', this._handlePaste);
        this._elements.editable[action]('blur', this._onChange);
        this._elements.editable[action]('keyup', this._onKeyUp);

        this._interactive = bool;

	}

	get interactive() {
		return !!this._interactive;
	}

	get intentToDelete() {
		return !!this._intentToDelete;
	}

	set intentToDelete(bool) {

		if (this._intentToDelete === bool) return;

		bool
            ? this._elements.editable.setAttribute(C.DELETABLE_ATTR, '')
            : this._elements.editable.removeAttribute(C.DELETABLE_ATTR, '');

        this._intentToDelete = bool;

	}

	_onKeyDown(e) {

		switch(e.which) {

			case C.KEY.BACKSPACE:

				if (!this._options.deletable) break;
				if (this._elements.editable.children.length > 0) break;
				if (this._elements.editable.firstChild && this._elements.editable.firstChild.nodeValue) break;

				if (this.intentToDelete) this._destroy();
				this.intentToDelete = true;

				return;

            case C.KEY.ENTER:
            	e.preventDefault();
            	this._options.allowBreaks && this._insertBreak();
                break;

            case C.KEY.TAB:
                if (!this._options.allowTabs) break;

				e.preventDefault();
				this._insertTab();
				break;

		}

        this.intentToDelete = false;

	}

    _handlePaste(e) {

        e.preventDefault();

        const text = (e.clipboardData || window.clipboardData)
            .getData('Text')
            .replace(/(?:\r\n|\r|\n)/g, '<br>')
            .replace(new RegExp(CHAR_TAB, 'g'), CHAR_4NBSP);

        this._insertAtCaret(text);

    }

	_onChange() {
        clearTimeout(this._stoppedTypingTimeout);
        dispatchEvent(this._elements.self, 'change');
	}

    _onKeyUp() {
		clearTimeout(this._stoppedTypingTimeout);
		this._stoppedTypingTimeout = setTimeout(this._onChange, 2000);
	}

	_insertBreak() {
        this._insertAtCaret('<br>');
	}

	_insertTab() {
        this._insertAtCaret(CHAR_4NBSP);
	}

	_insertAtCaret(text) {

		const range = this._getRange();

		if (range) {

            range.deleteContents();

            const wrapper = document.createElement('div');
            wrapper.innerHTML = text;

            const lastNode = wrapper.lastChild;

            Array.from(wrapper.childNodes)
				.reverse()
				.forEach(node => range.insertNode(node));

            if(!this._elements.editable.lastChild.nodeValue) {
                this._elements.editable.lastChild.nodeValue = '\u00A0';
            }

			const newRange = this._getRange().cloneRange();
			newRange.collapse();
			newRange.setStartAfter(lastNode);

			const selection = window.getSelection();
			selection.removeAllRanges();
			selection.addRange(newRange);

		}
	}

	_getRange() {

        const selection = window.getSelection();

        if (selection.getRangeAt && selection.rangeCount) {
            return selection.getRangeAt(0);
        }

	}

	_destroy() {
        this.unload();
        dispatchEvent(this._elements.self, 'destroy');
	}

	unload() {
		this.editable = false;
		this.interactive = false;
	}

}

export default Editable;
