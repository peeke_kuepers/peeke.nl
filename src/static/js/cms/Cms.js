import Editable from 'cms/Editable';
import Input from 'cms/Input';
import Service from 'cms/Service';
import Collection from 'cms/Collection';
import C from 'cms/Constants';

class Cms {
	
	constructor(element) {
		 
		this._element = element;
        this._element.setAttribute(C.CMS_ENABLED_ATTR, '');

        this._inputs = new WeakMap();

		this._onNewNode = this._onNewNode.bind(this);
        this._onChange = this._onChange.bind(this);
        this._onDestroy = this._onDestroy.bind(this);

		this._element.addEventListener('newnode', this._onNewNode);
		this._element.addEventListener('change', this._onChange);
		this._element.addEventListener('destroy', this._onDestroy);

        this._initializeNode(element);

	}

	_initializeNode(element) {

		const elements = Array.from(element.querySelectorAll(`[${ C.PROPERTY_ATTR }]`));

		element.hasAttribute(C.PROPERTY_ATTR) && elements.push(element);

		elements.forEach(element => this._initializeInput(element));

	}

	_initializeInput(element) {

        element.addEventListener('change', this._onChange);

        const component = element.hasAttribute(C.ID_ATTR)
			? element
			: element.closest(`[${ C.ID_ATTR }]`);

        this._inputs.set(element, {
            controller: this._initializeInputController(element),
            id: component.getAttribute(C.ID_ATTR),
            property: element.getAttribute(C.PROPERTY_ATTR),
            type: component.getAttribute(C.TYPE_ATTR),
            component
        });
		
	}

	_initializeInputController(element) {

        if (element.tagName.toLowerCase() === 'input') {
            return new Input(element);
        }

        if (element.matches(`[${ C.COLLECTION_ATTR }]`)) {

            return new Collection(element, {
                allowedTypes: element.getAttribute(C.ALLOWED_TYPES_ATTR).split(',')
            });

        }

        return new Editable(element, {
            deletable: !!element.closest(`[${ C.COLLECTION_ATTR }]`),
            allowBreaks: element.hasAttribute(C.ALLOW_BREAKS_ATTR),
            allowTabs: element.hasAttribute(C.ALLOW_TABS_ATTR),
            editable: element.hasAttribute(C.EDITABLE_ELEMENT)
                ? element.querySelector(element.getAttribute(C.EDITABLE_ELEMENT))
                : element
        });

	}

    _onNewNode(e) {
		this._initializeNode(e.target);
    }

	_onChange(e) {

		if (!this._inputs.has(e.target)) return;

		e.stopPropagation();

		const input = this._inputs.get(e.target);
		const updates = {};

		updates[input.property] = input.controller.value;

		Service.updateComponent(input.type, input.id, updates);

	}

	_onDestroy(e) {

		e.stopPropagation();

        const input = this._inputs.get(e.target);

        Service.deleteComponent(input.type, input.id);

		input.component.parentNode.removeChild(input.component);

	}
	
}

export default Cms;