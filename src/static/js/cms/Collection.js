import Service from 'cms/Service'
import Orderable from 'cms/Orderable'
import C from 'cms/Constants'
import { nodeFromHtml } from 'util/domHelpers'
import { dispatchEvent } from 'util/eventHelpers'

class Collection {

  constructor (element, options) {

    this._options = Object.assign(Collection.options, options)

    this._element = element
    this._orderable = new Orderable(element)

    this._createComponent = this._createComponent.bind(this)
    this._onChange = this._onChange.bind(this)

    this._initialize()
      .then(() => this.interactive = true)

  }

  static get options () {
    return {
      allowedTypes: []
    }
  }

  get value () {
    return this._element.getAttribute(C.VALUE_ATTR).split(',')
  }

  set value (value) {
    this._element.setAttribute(C.VALUE_ATTR, typeof value === 'object' ? value.join(',') : value)
  }

  set interactive (bool) {

    if (this.interactive === bool) return

    const action = bool ? 'addEventListener' : 'removeEventListener'

    this._element[action]('orderchange', this._onChange)
    this._controls[action]('submit', this._createComponent)
    this._interactive = bool

  }

  get interactive () {
    return !!this._interactive
  }

  _initialize () {
    console.log('render collection controls for ', this._options.allowedTypes)
    return Service.renderComponent('components/collection', {allowedTypes: this._options.allowedTypes})
      .then(html => nodeFromHtml(html))
      .then(node => {
        this._element.appendChild(node)
        this._controls = node
      })

  }

  _createComponent (e) {

    e.preventDefault()

    const type = this._controls.querySelector('[name=type]').value

    return Service.createComponent(type)
      .then(component => {
        const template = `${ component.type.toLowerCase() }s/${ component.discriminator.toLowerCase() }`
        return Service.renderComponent(template, component)
      })
      .then(html => {
        const node = nodeFromHtml(html)
        this._controls.parentNode.insertBefore(node, this._controls)
        return node
      })
      .then(node => dispatchEvent(node, 'newnode'))
      .then(this._onChange)

  }

  _onChange () {

    console.log('something changed')

    const components = Array.from(this._element.children).filter(child => child.matches(`[${ C.ID_ATTR }]`))
    const refs = components.map(item => item.getAttribute(C.ID_ATTR))

    this.value = refs.join(',')

    dispatchEvent(this._element, 'change')

  }

  unload () {
    this.interactive = false
    this._orderable.unload()
    this._controls.parentNode.removeChild(this._controls)
  }

}

export default Collection