import parseOptions from 'util/parseOptions';

class Popup {

    constructor(element) {

        this._element = element;
        this._options = parseOptions(Popup, element);
        this._triggers = Array.from(document.querySelectorAll(this._options.trigger));

        this._handleTrigger = this._handleTrigger.bind(this);

        this.visible = false;

        this._bindEvents();

    }

    static get options() {
        return {
            trigger: ''
        }
    }

    static get parse() {
        return {
            trigger: s => s
        }
    }

    set visible(bool) {
        this._element.setAttribute('aria-hidden', !bool);
    }

    get visible() {
        return this._element.getAttribute('aria-hidden') === 'false';
    }

    _bindEvents() {
        this._triggers.forEach(trigger => trigger.addEventListener('click', this._handleTrigger));
    }

    _handleTrigger() {
        this.visible = !this.visible;
    }

}

export default Popup;