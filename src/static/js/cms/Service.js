const serialize = (obj = {}) => Object.keys(obj)
	.map(key => [key, obj[key]].join('='))
	.join('&');

const jsonHeaders = () => new Headers({
    'Accept': 'application/json',
	'Content-Type': 'application/json'
});

class Service {
	
	static createComponent(type) {

		const uri = '/api/create/component/' + type;

		const request = new Request(uri, {
			method: 'POST',
			headers: jsonHeaders()
		});

		return fetch(request)
			.then(response => response.json())
			.then(response => response.component);

	}

	static renderComponent(component, data) {
		return fetch(`/api/render/partial/${ component }?${ serialize(data) }`)
			.then(response => response.json())
			.then(data => data.html);
	}

	static updateComponent(type, id, updates) {

		const uri = `/api/update/${ type }/${ id }`;

		console.log(uri);
		console.log('update:', updates);

		const request = new Request(uri, {
			method: 'POST',
			headers: jsonHeaders(),
			body: JSON.stringify(updates)
		});

		return fetch(request)
			.then(response => response.json())
			.then(response => response.status === 200 ? Promise.resolve() : Promise.reject(new Error('Status not OK')))
			.catch(err => console.warn(err));

	}

	static deleteComponent(type, id) {

		const uri = `/api/delete/${ type }/${ id }`;

		const request = new Request(uri, {
			method: 'POST',
			headers: jsonHeaders()
		});

		return fetch(request)
			.then(response => response.json())

	}
	
}

export default Service;