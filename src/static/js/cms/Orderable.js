import C from 'cms/Constants';
import { raf, dispatchEvent } from 'util/eventHelpers';

class Orderable {

	constructor(element) {

		this._element = element;
		this._droparea = this._setupDroparea();

		this._dragging = false;
		this._allowDrag = false;
		this._mouse = {};
		this._target = {};
		this._dragDelay = null;
		this._dragTarget = null;

		this._onMouseDown = this._onMouseDown.bind(this);
		this._onMouseUp = this._onMouseUp.bind(this);
		this._onMouseMove = this._onMouseMove.bind(this);
		this._onKeyDown = this._onKeyDown.bind(this);
		this._onKeyUp = this._onKeyUp.bind(this);

		this.interactive = true;

	}

    set interactive(bool) {

		if (this.interactive === bool) return;

        const action = bool ? 'addEventListener' : 'removeEventListener';

        window[action]('mousedown', this._onMouseDown);
        window[action]('mouseup', this._onMouseUp);
        window[action]('mousemove', raf(this._onMouseMove));
        window[action]('keydown', this._onKeyDown);
        window[action]('keyup', this._onKeyUp);

        this._interactive = bool;

    }

    get interactive() {
        return !!this._interactive;
    }

    set dragging(bool) {

        this._dragging = bool;

        if (bool) {
            this._dragTarget.setAttribute(C.DRAGGING_ATTR, '');
            this._droparea.style.opacity = 1;
            this._updateDragTarget();
        } else {
            this._dragTarget.removeAttribute(C.DRAGGING_ATTR);
            this._dragTarget.removeAttribute('style');
            this._droparea.style.opacity = 0;
        }

    }

    get dragging() {
        return this._dragging;
    }
	
	_setupDroparea() {
		
		const droparea = document.createElement('div');
		droparea.setAttribute(C.DROPAREA_ATTR, '');
		
		this._element.appendChild(droparea);

		return droparea;

	}

	_onKeyDown(e) {
		if (e.which === C.KEY.SHIFT) {
			this._allowDrag = true;
		}
	}

	_onKeyUp(e) {
        if (e.which === C.KEY.SHIFT) {
            this._allowDrag = false;
        }
	}

	_onMouseDown(e) {

		if (!this._allowDrag) return;

		if (e.target.closest(`[${ C.COLLECTION_ATTR }]`) !== this._element.closest(`[${ C.COLLECTION_ATTR }]`)) return;
		console.log(e.target.closest(`[${ C.COLLECTION_ATTR }]`), this._element.closest(`[${ C.COLLECTION_ATTR }]`));

		this._children = Array.from(this._element.children).filter(child => child !== this._droparea);

		const dragTarget = this._children.filter(child => child.matches(`[${ C.ID_ATTR }]:hover`)).pop();

		if (!dragTarget) return;

		this._dragTarget = dragTarget;
		this._dropareaIndex = this._children.indexOf(dragTarget);
		this._mouse = { x: e.clientX, y: e.clientY };
		this._origin = { x: -e.clientX, y: -e.clientY - window.scrollY };

		this._dragDelay = setTimeout(() => this.dragging = true, 100);

	}

	_onMouseUp() {

		if (!this.dragging) return;

		if (this._dropareaIndex !== this._children.indexOf(this._dragTarget)) {
			this._reorder(this._dragTarget, this._dropareaIndex);
			dispatchEvent(this._element, 'orderchange');
		}

		this.dragging = false;
		clearTimeout(this._dragDelay);

	}

	_onMouseMove(e) {

		if (!this.dragging) return;

		this._mouse = { x: e.clientX, y: e.clientY };
		this._updateDragTarget();

	}

	_updateDragTarget() {

		const x = this._origin.x + this._mouse.x;
		const y = this._origin.y + this._mouse.y + window.scrollY;
		
		this._dragTarget.style.transform = `translateX(${ x }px) translateY(${ y }px)`;

		const dropareaIndex = this._getDropareaIndex();

		if (this._dropareaIndex !== dropareaIndex) {
			this._dropareaIndex = dropareaIndex;
			this._positionDroparea(dropareaIndex);
		}
		
	}

	_getDropareaIndex() {

		const distances = this._children.map(child => Math.abs(child.getBoundingClientRect().top - this._mouse.y));
		const indexOfCurrent = this._children.indexOf(this._dragTarget);
		const indexOfClosest = distances.reduce((indexOfClosest, distance, index) => {

			const closestDistance = distances[indexOfClosest] || Infinity;

			if (distance < closestDistance && index !== indexOfCurrent) {
				return index;
			}

			return indexOfClosest;

		}, -1);

		return Math.min(indexOfClosest, this._children.filter(child => child.matches(`[${ C.ID_ATTR }]`)).length);

	}

	_positionDroparea(index) {
		const dropareaY = this._children[index].getBoundingClientRect().top - this._element.getBoundingClientRect().top;
		this._droparea.style.transform = `translateY(${ dropareaY }px)`;
	}

	_reorder(target, index) {
		this._element.insertBefore(target, this._children[index]);
	}

	unload() {

		this.interactive = false;
        this.dragging = false;

        clearTimeout(this._dragDelay);

	}

}

export default Orderable;
