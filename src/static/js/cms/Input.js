import { getInputValue, setInputValue } from 'util/domHelpers';

class Input {

	constructor(element) {
		this._element = element;
	}

	set value(value) {
        setInputValue(this._element, value);
	}

	get value() {
		return getInputValue(this._element);
	}

}

export default Input;