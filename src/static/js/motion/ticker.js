import observer from 'util/observer';

class Ticker {

    constructor() {
        this._time = 0;
        this._resolution = 1000/60;
        this._renderDebt = 0;
        this._bindEvents();
    }

    _bindEvents() {

        if ('requestAnimationFrame' in window) {
            this._raf = m => requestAnimationFrame(t => m(t));
        } else {
            this._raf = m => setTimeout(() => m(this._time + 1000 / 60), 1000 / 60);
        }

        this._tick(0);

    }

    _tick(t) {

        this._renderDebt += t - this._time;
        if (this._renderDebt > this._resolution * 2) {
            this._renderDebt = this._resolution * 2;
        }

        while(this._renderDebt >= this._resolution) {
            observer.publish(this, 'tick', this._resolution);
            this._renderDebt -= this._resolution;
        }

        observer.publish(this, 'time', t);

        this._time = t;
        this._raf(t => this._tick(t));

    }

    get time() {
        return this._time;
    }

}

export default new Ticker();
