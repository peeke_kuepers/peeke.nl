const animate = function(from, to, ...rest) {

  const easeGiven = rest.length === 3;
  const ease = easeGiven ? rest[0] : t => t;
  const duration = easeGiven ? rest[1] : rest[0];
  const cb = easeGiven ? rest[2] : rest[1];

  const delta = to - from;

  let time;

  return new Promise(resolve => {

    const tick = d => {

      if (!time) {
        time = d;
      }

      const f = (d - time) / duration;

      if (f >= 1) {
        cb(to);
        resolve();
        return;
      }

      cb(from + ease(f) * delta);

      window.requestAnimationFrame(tick);

    };

    window.requestAnimationFrame ? window.requestAnimationFrame(tick) : cb(to);

  })

};

export default animate;
