function easeInQuad(pos) {
    return Math.pow(pos, 2);
}

function easeOutQuad(pos) {
    return -(Math.pow((pos-1), 2) -1);
}

function easeInOutQuad(pos) {
    if ((pos/=0.5) < 1) return 0.5*Math.pow(pos,2);
    return -0.5 * ((pos-=2)*pos - 2);
}

function easeInCubic(pos) {
    return Math.pow(pos, 3);
}

function easeOutCubic(pos) {
    return (Math.pow((pos-1), 3) +1);
}

function easeInOutCubic(pos) {
    if ((pos/=0.5) < 1) return 0.5*Math.pow(pos,3);
    return 0.5 * (Math.pow((pos-2),3) + 2);
}

function easeInQuart(pos) {
    return Math.pow(pos, 4);
}

function easeOutQuart(pos) {
    return -(Math.pow((pos-1), 4) -1);
}

function easeInOutQuart(pos) {
    if ((pos/=0.5) < 1) return 0.5*Math.pow(pos,4);
    return -0.5 * ((pos-=2)*Math.pow(pos,3) - 2);
}

function easeInQuint(pos) {
    return Math.pow(pos, 5);
}

function easeOutQuint(pos) {
    return (Math.pow((pos-1), 5) +1);
}

function easeInOutQuint(pos) {
    if ((pos/=0.5) < 1) return 0.5*Math.pow(pos,5);
    return 0.5 * (Math.pow((pos-2),5) + 2);
}

function easeInSine(pos) {
    return -Math.cos(pos * (Math.PI/2)) + 1;
}

function easeOutSine(pos) {
    return Math.sin(pos * (Math.PI/2));
}

function easeInOutSine(pos) {
    return (-0.5 * (Math.cos(Math.PI*pos) -1));
}

function easeInExpo(pos) {
    return (pos===0) ? 0 : Math.pow(2, 10 * (pos - 1));
}

function easeOutExpo(pos) {
    return (pos===1) ? 1 : -Math.pow(2, -10 * pos) + 1;
}

function easeInOutExpo(pos) {
    if(pos===0) return 0;
    if(pos===1) return 1;
    if((pos/=0.5) < 1) return 0.5 * Math.pow(2,10 * (pos-1));
    return 0.5 * (-Math.pow(2, -10 * --pos) + 2);
}

function easeInCirc(pos) {
    return -(Math.sqrt(1 - (pos*pos)) - 1);
}

function easeOutCirc(pos) {
    return Math.sqrt(1 - Math.pow((pos-1), 2));
}

function easeInOutCirc(pos) {
    if((pos/=0.5) < 1) return -0.5 * (Math.sqrt(1 - pos*pos) - 1);
    return 0.5 * (Math.sqrt(1 - (pos-=2)*pos) + 1);
}

function easeOutBounce(pos) {
    if ((pos) < (1/2.75)) {
        return (7.5625*pos*pos);
    } else if (pos < (2/2.75)) {
        return (7.5625*(pos-=(1.5/2.75))*pos + 0.75);
    } else if (pos < (2.5/2.75)) {
        return (7.5625*(pos-=(2.25/2.75))*pos + 0.9375);
    } else {
        return (7.5625*(pos-=(2.625/2.75))*pos + 0.984375);
    }
}

function easeInBack(pos) {
    var s = 1.70158;
    return (pos)*pos*((s+1)*pos - s);
}

function easeOutBack(pos) {
    var s = 1.70158;
    return (pos=pos-1)*pos*((s+1)*pos + s) + 1;
}

function easeInOutBack(pos) {
    var s = 1.70158;
    if((pos/=0.5) < 1) return 0.5*(pos*pos*(((s*=(1.525))+1)*pos -s));
    return 0.5*((pos-=2)*pos*(((s*=(1.525))+1)*pos +s) +2);
}

function elastic(pos) {
    return -1 * Math.pow(4,-8*pos) * Math.sin((pos*6-1)*(2*Math.PI)/2) + 1;
}

function swingFromTo(pos) {
    var s = 1.70158;
    return ((pos/=0.5) < 1) ? 0.5*(pos*pos*(((s*=(1.525))+1)*pos - s)) :
    0.5*((pos-=2)*pos*(((s*=(1.525))+1)*pos + s) + 2);
}

function swingFrom(pos) {
    var s = 1.70158;
    return pos*pos*((s+1)*pos - s);
}

function swingTo(pos) {
    var s = 1.70158;
    return (pos-=1)*pos*((s+1)*pos + s) + 1;
}

function bounce(pos) {
    if (pos < (1/2.75)) {
        return (7.5625*pos*pos);
    } else if (pos < (2/2.75)) {
        return (7.5625*(pos-=(1.5/2.75))*pos + 0.75);
    } else if (pos < (2.5/2.75)) {
        return (7.5625*(pos-=(2.25/2.75))*pos + 0.9375);
    } else {
        return (7.5625*(pos-=(2.625/2.75))*pos + 0.984375);
    }
}

function bouncePast(pos) {
    if (pos < (1/2.75)) {
        return (7.5625*pos*pos);
    } else if (pos < (2/2.75)) {
        return 2 - (7.5625*(pos-=(1.5/2.75))*pos + 0.75);
    } else if (pos < (2.5/2.75)) {
        return 2 - (7.5625*(pos-=(2.25/2.75))*pos + 0.9375);
    } else {
        return 2 - (7.5625*(pos-=(2.625/2.75))*pos + 0.984375);
    }
}

function easeFromTo(pos) {
    if ((pos/=0.5) < 1) return 0.5*Math.pow(pos,4);
    return -0.5 * ((pos-=2)*Math.pow(pos,3) - 2);
}

function easeFrom(pos) {
    return Math.pow(pos,4);
}

function easeTo(pos) {
    return Math.pow(pos,0.25);
}


export {
    easeInQuad,
    easeOutQuad,
    easeInOutQuad,
    easeInCubic,
    easeOutCubic,
    easeInOutCubic,
    easeInQuart,
    easeOutQuart,
    easeInOutQuart,
    easeInQuint,
    easeOutQuint,
    easeInOutQuint,
    easeInSine,
    easeOutSine,
    easeInOutSine,
    easeInExpo,
    easeOutExpo,
    easeInOutExpo,
    easeInCirc,
    easeOutCirc,
    easeInOutCirc,
    easeOutBounce,
    easeInBack,
    easeOutBack,
    easeInOutBack,
    elastic,
    swingFromTo,
    swingFrom,
    swingTo,
    bounce,
    bouncePast,
    easeFromTo,
    easeFrom,
    easeTo
};

