/*
 * Blatantly adapted from https://codepen.io/jlfwong/pen/PzKbyg
 * */

class MetaBalls {

  constructor (canvas, options = {size: 10}) {

    this._gl = canvas.getContext('webgl')

    this._size = options.size
    this._width = canvas.width
    this._height = canvas.height

    const program = this._gl.createProgram()
    this._gl.attachShader(program, this.vertexShader)
    this._gl.attachShader(program, this.fragmentShader)
    this._gl.linkProgram(program)
    this._gl.useProgram(program)

    const vertexData = new Float32Array([
      -1.0, 1.0,
      -1.0, -1.0,
      1.0, 1.0,
      1.0, -1.0,
    ])
    const vertexDataBuffer = this._gl.createBuffer()

    this._gl.bindBuffer(this._gl.ARRAY_BUFFER, vertexDataBuffer)
    this._gl.bufferData(this._gl.ARRAY_BUFFER, vertexData, this._gl.STATIC_DRAW)

    const positionHandle = this.getAttribLocation(program, 'position')
    this._gl.enableVertexAttribArray(positionHandle)
    this._gl.vertexAttribPointer(positionHandle, 2, this._gl.FLOAT, this._gl.FALSE, 2 * 4, 0)

    this._metaballsHandle = this.getUniformLocation(program, 'metaballs')

  }

  // Utility to fail loudly on shader compilation failure
  compileShader (shaderSource, shaderType) {
    const shader = this._gl.createShader(shaderType)
    this._gl.shaderSource(shader, shaderSource)
    this._gl.compileShader(shader)

    if (!this._gl.getShaderParameter(shader, this._gl.COMPILE_STATUS)) {
      throw 'Shader compile failed with: ' + this._gl.getShaderInfoLog(shader)
    }

    return shader
  }

  get vertexShader () {
    return this.compileShader(`
        
      attribute vec2 position;

      void main() {
        // position specifies only x and y.
        // We set z to be 0.0, and w to be 1.0
        gl_Position = vec4(position, 0.0, 1.0);
      }
      
    `, this._gl.VERTEX_SHADER)
  }

  get fragmentShader () {
    return this.compileShader(`
    
      precision highp float;
      uniform vec3 metaballs[${ this._size }];
      const float WIDTH = ${ this._width }.0;
      const float HEIGHT = ${ this._height }.0;
      
      void main(){
        float x = gl_FragCoord.x;
        float y = gl_FragCoord.y;
        float v = 0.0;
        for (int i = 0; i < ${ this._size }; i++) {
          vec3 mb = metaballs[i];
          float dx = mb.x - x;
          float dy = mb.y - y;
          float r = mb.z;
          v += r*r/(dx*dx + dy*dy);
        }
        if (v > 1.0) {
          gl_FragColor = (1.05 * vec4(0.73, 0.04, 0.35, 1.0) + (min(v, 2.0) - 0.95) * vec4(0.93, 0.09, 0.39, 1.0)) / min(v, 2.0);
        } else {
          gl_FragColor = vec4(0.0, 0.0, 0.0, 0);
        }
      }
      
  `, this._gl.FRAGMENT_SHADER)
  }

  // Utility to complain loudly if we fail to find the attribute

  getAttribLocation (program, name) {
    const attributeLocation = this._gl.getAttribLocation(program, name)
    if (attributeLocation === -1) {
      throw 'Can not find attribute ' + name + '.'
    }
    return attributeLocation
  }

  // Utility to complain loudly if we fail to find the uniform
  getUniformLocation (program, name) {
    const uniformLocation = this._gl.getUniformLocation(program, name)
    if (uniformLocation === -1) {
      throw 'Can not find uniform ' + name + '.'
    }
    return uniformLocation
  }

  /**
   * Simulation step, data transfer, and drawing
   */

  render (x, y, r) {

    // To send the data to the GPU, we first need to
    // flatten our data into a single array.
    const dataToSendToGPU = new Float32Array(3 * this._size)
    for (let i = 0; i < this._size; i++) {
      dataToSendToGPU[i * 3] = x[i]
      dataToSendToGPU[i * 3 + 1] = this._height - y[i]
      dataToSendToGPU[i * 3 + 2] = r[i]
    }

    this._gl.uniform3fv(this._metaballsHandle, dataToSendToGPU)
    this._gl.drawArrays(this._gl.TRIANGLE_STRIP, 0, 4)

  }

  destroy() {
    // todo: implement
  }
}

export default MetaBalls