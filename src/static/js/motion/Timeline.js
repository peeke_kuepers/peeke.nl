import observer from 'util/observer';
import ticker from 'motion/ticker';

class Timeline {

    /**
     * Returns a timeline running from 0 to 1.
     * Start and end are defined relative to the moment of creation of the timeline
     * Provide arguments as new Timeline(end, fn) or new Timeline(start, end, fn)
     * @returns {{start: int, end: int, duration: duration, started: boolean, fn: function}}
     */

    constructor() {

        let start = arguments.length === 3 ? arguments[0] : 0;
        let end = arguments.length === 3 ? arguments[1] : arguments[0];
        let fn = arguments[arguments.length - 1];

        this._start = ticker.time + start + 16;
        this._end = ticker.time + end;
        this._duration = end - start - 16;
        this._running = false;
        this._fn = fn;

		this._ended = new Promise(resolve => {
			this._resolveEnded = resolve;
		});

        this._tick = this._tick.bind(this);
        this._tick(16);

        observer.subscribe(ticker, 'time', this._tick);

    }

    then(...args) {
        return this._ended.then(...args);
    }

    cancel() {
        this._running = false;
        observer.unsubscribe(ticker, 'time', this._tick);
    }

    _tick(time) {

        if (time < this._start) {
            return; // Bail out
        } else if (!this._running) {
            this._running = true;
            this._fn(0);
        }

        if (time > this._end) {
            this.cancel();
            this._fn(1);
			this._resolveEnded();
            return; // Bail out
        }

        this._fn((time - this._start) / this._duration);

    }

}

export default Timeline;
