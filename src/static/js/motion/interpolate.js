const interpolate = (start, stop, step = 1) => {
  const steps = Math.round((stop - start) / step)
  const dir = steps ? Math.abs(steps) / steps : 0
  return Array(Math.abs(steps) + 1)
    .fill(start)
    .map((v, i) => v + step * i * dir)
}

export default interpolate