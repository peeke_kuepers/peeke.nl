import observer from 'util/observer';

class VisibilityMonitor {

    constructor(element) {

        this._onIntersectionChange = this._onIntersectionChange.bind(this);
        this._observer = new IntersectionObserver(this._onIntersectionChange);
        this._observer.observe(element);

        this._visible = null;

    }

    get visible() {
        return this._visible;
    }

    set visible(bool) {
        if (this._visible === bool) return;
        this._visible = bool;
        observer.publish(this, 'visibilitychange', bool);
    }

    _onIntersectionChange(entries) {
        this.visible = entries[0].intersectionRatio > 0;
    }

}

export default VisibilityMonitor;
