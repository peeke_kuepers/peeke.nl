const MODULE_ATTR = 'data-module'

const arrayFrom = stuff => [].slice.call(stuff)
const unique = arr => arr
  .sort()
  .filter((item, pos, ary) => !pos || item != ary[pos - 1])

let preload = []

class Loader {

  constructor () {
    this.isReady = Promise.resolve()
  }

  require (path) {
    return System.import(path)
    // todo: support other module loaders
  }

  parse (elem) {

    this._elements = Array.from(elem.querySelectorAll('[' + MODULE_ATTR + ']'))
    if (elem.matches && elem.matches('[' + MODULE_ATTR + ']')) {
      this._elements.push(elem)
    }

    this._paths = this._elements.map(e => e.getAttribute(MODULE_ATTR))

    this._preload(preload, this._paths)

    // Change

    const modules = this._paths
      .map((path, i) => this.require(path)
        .then(module => new module.default(this._elements[i])))

    this._paths.forEach((path, i) => {
      this._updateInitialized(this._elements[i], path.split('/').pop())
    })

    this.isReady = Promise.all(modules).then(modules_ => this._modules = modules_)
    // todo: fix isReady when modules get loaded while the current batch isn't ready

    return this.isReady

  }

  getModules (elements, path) {

    elements = arrayFrom(typeof elements === 'string' ? document.querySelectorAll(elements) : elements)

    return this.isReady.then(() => arrayFrom(this._modules
      .map((module, i) => elements.indexOf(this._elements[i]) !== -1 ? module : false)
      .map((module, i) => !path || path === this._paths[i] ? module : false)
      .filter(module => !!module)))

  }

  getModulesByPath (path) {
    return this._getByPath(path, this._modules)
  }

  getElementsByPath (path) {
    return this._getByPath(path, this._elements)
  }

  _updateInitialized (element, name) {
    const initialized = element.hasAttribute('element')
      ? element.getAttribute('data-initialized').split(',')
      : []
    element.setAttribute('data-initialized', [name, ...initialized].join(','))
  }

  _getByPath (path, arr) {

    return this.isReady.then(() => arr
      .map((item, i) => this._paths[i] === path ? item : false)
      .filter(item => !!item))

  }

  _preload (arr, paths) {

    const preload = paths.reduce((result, path) => result.concat(this._findDependencies(path)), [])
    const modules = unique(preload.concat(paths))
      .filter(path => paths.indexOf(path) === -1)
      .filter(path => arr.indexOf(path) === -1)

    modules.forEach(path => this.require(path))

  }

  _findDependencies (path) {
    if (!moduleGraph[path]) return []
    const deps = moduleGraph[path]
    return deps.length
      ? deps.reduce((result, dep) => result.concat(this._findDependencies(dep)), deps)
      : deps
  }
}

export default new Loader()
