import Timeline from 'motion/Timeline';
import measures from 'util/measures';
import observer from 'util/observer';
import { easeInOutCubic as ease } from 'vendor/es6/eases/eases';

class Bounds {

	constructor(options) {

		this._options = Object.assign({
			provideEasing: true
		}, options);

		Object.freeze(this._options);

		this._min = 0;
		this._max = measures.min * .1;
		this._radius = 0;

		observer.subscribe(measures, 'measureschange', this._onMeasuresChange.bind(this));

	}

	get min() {
		return this._min;
	}

	get max() {
		return this._max;
	}

	get radius() {
		return this._radius;
	}

	animateOut() {

		let minStart = this.min,
			maxStart = this.max,
			min = Math.sqrt(Math.pow(.5 * measures.max, 2) + Math.pow(.5 * measures.min, 2)),
			max = min + measures.min * .05;

		if (!this._options.provideEasing) {
			this._max = max;
			this._min = min;
			this._radius = max;
			observer.publish(this, 'radiuschange', this._radius);
			return Promise.resolve();
		}

		return new Promise(resolve => {

			new Timeline(0, 500, t => {

				let _t = ease(t);

				this._max = maxStart + _t * (max - maxStart);
				this._min = minStart + _t * (min - minStart);
				this._radius = minStart + _t * (max - minStart);
				observer.publish(this, 'radiuschange', this._radius);

				if (t === 1) {
					resolve();
				}

			});

		});

	}

	animateIn() {

		let max = Math.sqrt(Math.pow(.5 * measures.max, 2) + Math.pow(.5 * measures.min, 2)),
			min = .35 * measures.min;

		if (!this._options.provideEasing) {
			this._max = max;
			this._min = min;
			this._radius = max / 2;
			observer.publish(this, 'radiuschange', this._radius);
			return Promise.resolve();
		}

		return new Promise(resolve => {

			new Timeline(200, 800, t => {

				let _t = ease(t);

				this._max = _t * max;
				this._min = _t * min;
				this._radius = _t * max / 2;
				observer.publish(this, 'radiuschange', this._radius);

				if (t === 1) {
					resolve();
				}

			});

		});

	}

	_onMeasuresChange() {
		this.animateIn();
	}

}

export default Bounds;
