import VisualBase from 'visuals/VisualBase'
import animate from 'motion/animate'
import { easeInOutQuart } from 'motion/eases'

import THREE from 'proxy/three'

const color1 = [18 / 255, 213 / 255, 165 / 255]
const color2 = [18 / 255, 213 / 255, 165 / 255]

const dotSelf = (x, y) => x * x + y * y
const withinCircle = (x, y, r) => dotSelf(x / (r * 2), y / (r * 2)) * 4 > 1

const circleTexture = size => {

  const canvas = document.createElement('canvas')
  canvas.width = size
  canvas.height = size

  const texture = new THREE.Texture(canvas);

  const context = canvas.getContext('2d')
  context.beginPath()
  context.arc(size / 2, size / 2, size / 2, 0, Math.PI * 2, false)
  context.closePath()
  context.fillStyle = 'white'
  context.fill()

  texture.needsUpdate = true;

  return texture

}

class SpringField extends VisualBase {

  constructor (element) {

    super(element)

    this._scale = .1
    this._size = {
      x: element.offsetWidth,
      y: element.offsetHeight
    }
    this._center = {
      x: this._size.x / 2,
      y: this._size.y / 2
    }
    this._mouse = false

    animate(0, 1, easeInOutQuart, 1000, r => this._scale = r)

    this.reveal()

  }

  reveal () {
    this.run()
  }

  hide () {
    animate(1, 0, easeInOutQuart, 1000, r => this._scale = r)
      .then(() => this._destroy())
  }

  // dt = deltaTime (s)
  _simulate (dt) {

    for (let i = 0; i < this._particleCount; i++) {

      const x = this._displayPositions[i * 3]
      const y = this._displayPositions[i * 3 + 1]

      const odx = this._pointPositions[i * 3] - x
      const ody = this._pointPositions[i * 3 + 1] - y

      this._pointVelocities[i * 2] += odx / 600
      this._pointVelocities[i * 2 + 1] += ody / 600

      const r = 1
      const f = Math.min(1, dotSelf(this._pointVelocities[i * 2] / r, this._pointVelocities[i * 2 + 1] / r))

      this._pointColors[i * 3] = (1 - f) * color1[0] + f * color2[0]
      this._pointColors[i * 3 + 1] = (1 - f) * color1[1] + f * color2[1]
      this._pointColors[i * 3 + 2] = (1 - f) * color1[2] + f * color2[2]

      if (this._mouse) {

        const mdx = this._mouse.x - x
        const mdy = this._mouse.y - y
        const mr = this._radius
        const mdist = 1 - Math.min(1, dotSelf(mdx / mr, mdy / mr) * 4 * .75)

        this._pointVelocities[i * 2] -= mdx / 200 * mdist
        this._pointVelocities[i * 2 + 1] -= mdy / 200 * mdist

      }

      this._displayPositions[i * 3] += this._pointVelocities[i * 2]
      this._displayPositions[i * 3 + 1] += this._pointVelocities[i * 2 + 1]

      this._pointVelocities[i * 2] *= .975
      this._pointVelocities[i * 2 + 1] *= .975

      this._cloudGeometry.attributes.position.needsUpdate = true;
      this._cloudGeometry.attributes.color.needsUpdate = true;

    }

  }

  _render () {
    this.renderer.render(this.scene, this.camera)
  }

  _setup () {

    super._setup()

    this._radius = this.vmin / 3
    this._particleDistance = 10 / window.devicePixelRatio
    this._particleCount = Math.PI * this._radius ** 2 / this._particleDistance ** 2

    this._cloudGeometry = this._generatePointCloudGeometry()

    this._cloudMesh = new THREE.Points(this._cloudGeometry, new THREE.PointsMaterial({
      size: 7 / window.devicePixelRatio,
      map: circleTexture(256),
      transparent: true,
      sizeAttenuation: false,
      vertexColors: THREE.VertexColors,
      depthWrite: false
    }))

    this.scene.add(this._cloudMesh)
    this.camera = this.perspectiveCamera

    this.scene.add(this.camera)

    this._onMousemove = this._onMousemove.bind(this)
    document.addEventListener('mousemove', this._onMousemove, {passive: true})

  }

  _generatePointCloudGeometry () {

    const geometry = new THREE.BufferGeometry()

    this._pointColors = new Float32Array(this._particleCount * 3)
    this._pointPositions = new Float32Array(this._particleCount * 3)
    this._pointVelocities = new Float32Array(this._particleCount * 2)

    let j = 0
    const r = this._radius / this._particleDistance
    const diameterSquared = (r * 2) ** 2

    for (let i = 0; i < diameterSquared; i++) {

      const x = (i % (r * 2) - r) * this._particleDistance
      const y = (Math.floor(i / (r * 2)) - r) * this._particleDistance

      if (withinCircle(x, y, this._radius)) continue

      this._pointPositions[j * 3] = x // x
      this._pointPositions[j * 3 + 1] = y // y
      this._pointPositions[j * 3 + 2] = 0 // z

      this._pointColors[j * 3] = color1[0] // x
      this._pointColors[j * 3 + 1] = color1[1] // y
      this._pointColors[j * 3 + 2] = color1[2] // z

      this._pointVelocities[j * 2] = 0 // x
      this._pointVelocities[j * 2 + 1] = 0 // y

      j++

    }

    this._particleCount = j
    this._displayPositions = this._pointPositions.slice(0, this._particleCount * 3)

    geometry.setDrawRange(0, this._particleCount)
    geometry.addAttribute('position', new THREE.BufferAttribute(this._displayPositions, 3).setDynamic(true))
    geometry.addAttribute('color', new THREE.BufferAttribute(this._pointColors, 3).setDynamic(true))

    geometry.attributes.position.needsUpdate = true;
    geometry.attributes.color.needsUpdate = true;

    return geometry

  }

  _onMousemove (e) {
    this._mouse = {x: (e.pageX - this._center.x), y: -(e.pageY - this._center.y)}
  }

}

export default SpringField