import 'vendor/zenparsing/zen-observable'
import THREE from 'proxy/three'

export const makePlaybackObservable = container => {
  return new Observable(observer => {

    let pageVisible = true
    let inViewport = true
    let pageHeight = window.innerHeight
    let shouldPlay = false

    const broadcast = () => {
      if (shouldPlay === pageVisible && inViewport) return
      shouldPlay = pageVisible && inViewport
      observer.next(shouldPlay ? 'PLAY' : 'PAUSE')
    }
    const onVisibilityChange = () => {
      pageVisible = !document.hidden
      broadcast()
    }
    const onScroll = () => {
      const y = window.pageYOffset || document.documentElement.scrollTop
      inViewport = y < pageHeight
      broadcast()
    }
    const onResize = () => pageHeight = window.innerHeight

    window.addEventListener('visibilitychange', onVisibilityChange)
    window.addEventListener('scroll', onScroll)
    window.addEventListener('resize', onResize)

    return () => {
      window.removeEventListener('visibilitychange', onVisibilityChange)
      window.removeEventListener('scroll', onScroll)
      window.removeEventListener('resize', onResize)
    }

  })
}

export const makeNormalizedMouseObservable = (element, options = { centered: true, inverted: true, applyDensity: true }) => {
  return new Observable(observer => {

    const density = options.applyDensity ? window.devicePixelRatio : 1
    const calculateCenter = () => ({
      x: element.offsetWidth / 2,
      y: element.offsetHeight / 2
    })

    let center = calculateCenter()

    window.addEventListener('resize', center = calculateCenter())
    document.addEventListener('mousemove', e => {
      const mouse = {
        x: e.pageX * density,
        y: e.pageY * density
      }
      if (options.centered) {
        mouse.x -= center.x * density
        mouse.y -= center.y * density
      }
      if (options.inverted) {
        mouse.y *= -1
      }
      observer.next(mouse)
    })

  })
}

export class Simulation {

  constructor (simulate, simulationFrame = 8) {
    this._running = null
    this._time = performance.now()
    this._simulationDebt = 0
    this._simulationFrame = simulationFrame
    this._simulate = simulate
  }

  play () {
    if (this._running) return
    this._running = true
    this._time = performance.now()
    requestAnimationFrame(() => this._update())
  }

  pause () {
    if (!this._running) return
    this._running = false
  }

  get isRunning() {
    return this._running
  }

  _update () {

    if (!this._running) return

    const now = performance.now()
    this._simulationDebt = this._simulationDebt + now - this._time
    this._time = now

    while (this._simulationDebt >= this._simulationFrame) {
      this._simulationDebt -= this._simulationFrame
      this._simulate(this._simulationFrame / 1000)
    }

    setTimeout(() => this._update(), this._simulationFrame)

  }

}

export class World {

  constructor (container, options = {camera: World.PERSPECTIVE_CAMERA, near: .1, far: 3000, fov: 45}) {

    this._container = container
    this._options = options

    this._renderer = new THREE.WebGLRenderer({antialias: true, alpha: true})
    this._renderer.setPixelRatio(window.devicePixelRatio)
    this._scene = new THREE.Scene()

    switch (options.camera) {
      case World.ORTHOGRAPHIC_CAMERA:
        this._camera = new THREE.OrthographicCamera(0, 0, 0, 0)
      case World.PERSPECTIVE_CAMERA:
      default:
        this._camera = new THREE.PerspectiveCamera(options.fov, 1, options.near, this._options.far)
    }

    this._scene.add(this._camera)
    this._container.appendChild(this._renderer.domElement)

    window.addEventListener('resize', () => this._updateCamera())

    this._updateCamera()

  }

  static PERSPECTIVE_CAMERA = 0
  static ORTHOGRAPHIC_CAMERA = 1

  render () {
    this.renderer.render(this._scene, this._camera)
  }

  destroy () {
    this._container.removeChild(this._renderer.domElement)
  }

  _updateCamera () {

    const [width, height] = this.size
    console.log('resize to ', width, height)

    if (this._options.camera === World.PERSPECTIVE_CAMERA) {

      this._camera.aspect = width / height
      this._camera.position.z = this.vmin / 2 / Math.tan(Math.PI / 180 * this._options.fov / 2)

    } else if (this._options.camera === World.ORTHOGRAPHIC_CAMERA) {

      this._camera.left = width / -2
      this._camera.right = width / 2
      this._camera.top = height / 2
      this._camera.bottom = height / -2
      this._camera.position.z = this.vmin

    }

    this._camera.updateProjectionMatrix()
    this._renderer.setSize(width, height)

  }

  get size () {

    if (!this._renderer.domElement) return [0, 0]

    const width = window.innerWidth // * window.devicePixelRatio
    const height = window.innerHeight // * window.devicePixelRatio

    return [width, height]

  }

  get vmin () {
    return Math.min(...this.size)
  }

  get vmax () {
    return Math.max(...this.size)
  }

  get radius () {
    return this.vmin / 3
  }

  get element () {
    return this._renderer.domElement
  }

  get renderer () {
    return this._renderer
  }

  get scene () {
    return this._scene
  }

  get camera () {
    return this._camera
  }

}