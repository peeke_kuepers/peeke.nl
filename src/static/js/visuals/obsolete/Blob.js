// import observer from 'util/observer';

import VisualBase from 'visuals/VisualBase';

import { canvas } from 'util/canvasHelpers';
import { circle, mod, angle, circum, distance } from 'math/mathHelpers';
import { px, rad, deg } from 'util/units';
import { throttle } from 'util/eventHelpers'
import measures from 'util/measures';
import THREE from 'proxy/three';

const TAU = Math.PI * 2;

class Blob extends VisualBase {

	constructor(element) {

		super(element);

        this._onResize = throttle(this._onResize.bind(this), 100);

        window.addEventListener('resize', this._onResize);

		this.reveal();

	}

	reveal() {
		this.run();
	}

	hide() {

	}

	_setup() {

		super._setup();

		this._radius = this.vmin / 3;

		this._resolution = Math.round(circum(this._radius));
		this._grabSize = 15;

		this._surface = this._createSurface(this._resolution);

		this._geometry = new THREE.Geometry();
		this._geometry.dynamic = true;

		this._calculateVertices(this._geometry);
		this._calculateFaces(this._geometry);
		this._updateFaceColors(this._geometry);

		this._geometry.computeFaceNormals();
		this._geometry.computeVertexNormals();
		this._geometry.computeBoundingSphere();

		this._mesh = new THREE.Mesh( this._geometry, new THREE.MeshBasicMaterial({ vertexColors: THREE.VertexColors, side: THREE.DoubleSide }));

		this.scene.add( this._mesh );
		this.camera = this.orthographicCamera;

		this.scene.add(this.camera);

		this.reveal();

	}

	_simulate(dt) {

        this._smoothen('r', 2);
        this._smoothen('vr', 1);
        this._applyForces(dt);
        this._applyVelocities(dt);
        this._interact();
        this._updateVertices(this._geometry);

	}

	_onResize() {
		const radius = this.vmin / 3;
		const delta = radius - this._radius;
        this._radius = radius;

        for (let i = 0; i < this._resolution; i++) {
            this._surface.r[i] += delta;
            this._surface.sample[i] += delta;
        }
	}

	_render() {
		this.renderer.render( this.scene, this.camera );
	}

	// _onRadiusChange(radius) {
    //
	// 	this._surface.d(this._radius - radius);
	// 	this._surface.release();
	// 	this._radius = radius;
    //
	// }

    _clamp(n) {
		this._memoized = this._memoized || [];
		if (!this._memoized[n]) {
            this._memoized[n] = mod(n, this._resolution);
		}
		return this._memoized[n];
    }

	_calculateVertices(geometry) {

		for (let i = 0; i <= this._resolution; i++) {
			geometry.vertices[i] = new THREE.Vector3(0, 0, 0);
		}

		this._updateVertices(geometry);

	};

	_calculateFaces(geometry) {

		geometry.vertices.forEach((vertice, i) => {

			if (!i) return;

			const j = i === geometry.vertices.length - 1 ? 1 : i + 1;

			geometry.faces.push(new THREE.Face3(0, i, j));

		});

	};

	_updateVertices(geometry) {

        for (let i = 1; i <= this._resolution; i++) {

			const a = (i - 1) / this._resolution * TAU;
			const r = this._surface.r[i - 1];

			geometry.vertices[i].x = Math.cos(a) * r;
			geometry.vertices[i].y = -Math.sin(a) * r;

		}

		geometry.verticesNeedUpdate = true;

	}

	_updateFaceColors(geometry) {

		const faceIndices = [ 'a', 'b', 'c' ];
		const colors = [];

		geometry.vertices.forEach((vertice, i) => {

			const color = new THREE.Color( 0xffffff );

			const f = Math.min(1, Math.pow(distance({ x: 0, y: 0}, vertice) / this._radius / 2, 2));
			const color1 = [50, 20, 100];
			const color2 = [230, 20, 85];

			color.setRGB( ...color1.map((color, i) => (f * color + (1 - f) * color2[i]) / 255) );
			colors[i] = color; // use this array for convenience

		});

		geometry.faces.forEach(face => {

			for( let j = 2; j >= 0; j-- ) {
				const vertexIndex = face[ faceIndices[ j ] ];

				if (face.vertexColors[ j ]) {
					face.vertexColors[ j ].set(colors[ vertexIndex ]);
				} else {
					face.vertexColors[ j ] = colors[ vertexIndex ];
				}
			}

		});

		geometry.colorsNeedUpdate = true;

	};

	_interact() {

		const [ width, height ] = this.size;
		const screenCenter = { x: width / 2, y: height / 2 };
		const mouse = { x: measures.mouse.x * window.devicePixelRatio, y: measures.mouse.y * window.devicePixelRatio};
		const n = Math.floor(this._clamp(angle(screenCenter, mouse) / TAU * this._resolution));
		const r = Math.max(this._radius * .35, distance(screenCenter, mouse)) * this._revealRadius;
        const grabSize = Math.round(this._grabSize / 2 / circum(this._surface.r[n]) * this._resolution);

		if (r - grabSize * 2 > this._surface.r[n]) return;

		const neighbours = this._neighbours(n, grabSize);

        neighbours.forEach((_n, i) => {
        	const grabR = r - Math.sin(i / neighbours.length * Math.PI) * grabSize;
        	const f = Math.sqrt(1 - Math.abs((i - grabSize) / grabSize));
			this._surface.r[_n] = Math.min(
                this._surface.r[_n],
            	grabR * f + this._surface.r[_n] * (1 - f)
			);
			this._surface.vr[_n] = 0;
        });

	}

	_createSurface(resolution) {

		const surface = {
			r: new Float32Array(resolution),
			vr: new Float32Array(resolution),
			sample: new Float32Array(resolution),
		};

		for (let i = 0; i < resolution; i++) {
			surface.r[i] = this._radius;
            surface.sample[i] = this._radius;
		}

		return surface;

	}

	_applyForces(dt) {

        let circ = 0;
        for (let i = 0; i < this._resolution; i++) {
        	circ += circum(this._surface.r[i]) / this._resolution;
        }

        for (let i = 0; i < this._resolution; i++) {

			const fCenter = 150 * (this._radius - this._surface.r[i]);
            const fCircum = 25 * (circum(this._radius) - circ);
            const fFriction = 2 * -this._surface.vr[i];
            this._surface.vr[i] += (fCenter + fCircum + fFriction) * dt;

        }

	}

	_applyVelocities(dt) {

		for (let i = 0; i < this._resolution; i++) {
			this._surface.r[i] += this._surface.vr[i] * dt;
		}

	}

	_smoothen(prop, size) {

        size = Math.min(size, Math.floor(this._resolution / 2));

        for (let i = 0; i < this._resolution; i++) {
        	this._surface.sample[i] = this._surface[prop][i];
        }

        for (let i = 0; i < this._resolution; i++) {

            const neighbours = this._neighbours(i, size);
			const value = neighbours.reduce((result, neighbour) => result + this._surface.sample[neighbour], 0);

            this._surface[prop][i] = value / neighbours.length;

        }

	}

	_neighbours(i, count) {

        return new Array(count * 2 + 1)
            .fill(0)
            .map((_, n) => this._clamp(i + n - count))

	}

}

export default Blob;