import { canvas, repeatDraw } from 'util/canvasHelpers';
import { bounds, distance, map, range, angle, circle } from 'math/mathHelpers';
import { px, rad } from 'util/units';
import { easeOutQuad as ease } from 'vendor/es6/eases/eases';
import observer from 'util/observer';
import measures from 'util/measures';
import ticker from 'motion/ticker';
import Timeline from 'motion/Timeline';
import Spring from 'motion/Spring';
import Bounds from 'visuals/Bounds';

class Visual2 {
	
	constructor(element) {
		this._element = element;
		this._options = element.dataset;
		this._bounds = new Bounds();
		this._spring = new Spring();

		Object.assign(this, canvas(element));

		this._ctx.fillStyle = this._color = '#16e594';
		this._center.y = window.innerHeight / 2;
		this._radius = 0;
		this._amp = 0;
		this._angle = 0;
		this._radius = 0;
		this._inside = true;
		setTimeout(() => this._inside = distance(measures.mouse, this._center) < this._bounds.radius, 1000);
		
		this._bindEvents();
		setTimeout(() => this._bounds.animateIn(true), 250);
		setTimeout(() => this._bounds.animateOut(true), 25000);
	}
	
	_bindEvents() {
		this._tick = this._tick.bind(this);
		observer.subscribe(ticker, 'tick', this._tick);
	}
	
	_tick() {
		this._update() && this._render();
	}
	
	_update() {

		let amp = distance(measures.mouse, this._center) - this._bounds.radius,
			boundAmp = Math[this._inside ? 'max' : 'min'](0, amp);

		if (boundAmp === this._spring.amp && this._radius === this._bounds.radius) {
			return;
		}

		if (Math.abs(amp) < 125 && amp === boundAmp) {
			this._spring.grab(boundAmp);
			this._angle = angle(this._center, measures.mouse);
		} else if (this._spring.grabbed) {
			this._spring.release(boundAmp);
			this._inside = distance(measures.mouse, this._center) < this._bounds.radius;
			this._animatePop(measures.mouse);
		}
		
		this._amp = this._spring.amp;
		this._radius = this._bounds.radius;

		return true;

	}
	
	_render() {
 
		this._ctx.clearRect(0, 0, this._ctx.canvas.width, this._ctx.canvas.height);
		this._ctx.beginPath();

		repeatDraw(-.5, .5, 100, n => {

			let f = Math.max((.475 - Math.abs(n)) * 2, 0),
				theta = rad(n * 360) + this._angle,
				point = circle(this._center, theta, this._radius + f * this._amp * Math.cos(rad(n * 360 * 2)));

			this._ctx.lineTo(px(point.x), px(point.y));

		});

		this._ctx.closePath();

		this._ctx.fillStyle = this._color;
		this._ctx.fill();
	}

	_animatePop(point) {

		new Timeline(50, 300, n => {
			Promise.resolve().then(() => {
				
				let target = {
					x: .75 * point.x + .25 * (point.x * (1 - n) + measures.mouse.x * n),
					y: .75 * point.y + .25 * (point.y * (1 - n) + measures.mouse.y * n)
				};
				
				n = ease(n);
				
				this._ctx.beginPath();
				this._ctx.arc(px(target.x), px(target.y), px(10 * Math.sin(n * rad(180))), 0, rad(360));
				this._ctx.closePath();
				this._ctx.fillStyle = this._inside ? '#fff' : this._color;
				this._ctx.fill();
				
			})
		});

	}
	
}

export default Visual2;
