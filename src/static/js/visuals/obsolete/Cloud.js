import VisualBase from 'visuals/VisualBase';
import Bounds from 'visuals/Bounds';
import observer from 'util/observer';

import Color from 'util/Color';
import { distance, point, moveDistanceFromPoint, bounds, reflect, reverse, unit } from 'math/mathHelpers';
import measures from 'util/measures';
import THREE from 'proxy/three';

const MAX_PARTICLES = 20000;

class Cloud extends VisualBase {

	constructor(element) {

		super(element);

		this._radius = this.vmin / 3;
		this._depth = 0;
		this._depthVelocity = 1.5;
		// this._bounds = new Bounds({ provideEasing: true });

		// observer.subscribe(this._bounds, 'radiuschange', this._updateRadius.bind(this));
		
		this.reveal();

	}

	reveal() {
		this.run();
		// this._bounds.animateIn();
	}

	hide() {
		// this._bounds
		// 	.animateOut()
		// 	.then(() => this._destroy());
	}

	// _updateRadius(radius) {
    //
	// 	this._radius = this.vmin / 2 * 2;
	// 	console.log(this._radius);
    //
	// 	const max = this._pointPositions.reduce((max, pos) => Math.max(max, pos), 0);
	// 	if (!max) return;
    //
	// 	const f = this._radius / max;
	// 	for (let i = 0; i < this._particleCount * 3; i++) {
	// 		this._pointPositions[i] *= f;
	// 	}
    //
	// }

	// dt = deltaTime (s)
	_simulate(dt) {

		if (!this._radius) return;

		const lensRadius = 1.05;
		const center = point(0, 0, 0);
		const [ width, height ] = this.size;
		const mouse = {
			x: (measures.mouse.x - width / 2 / window.devicePixelRatio) * window.devicePixelRatio,
			y: (height / 2 / window.devicePixelRatio - measures.mouse.y) * window.devicePixelRatio,
			z: this._depth * window.devicePixelRatio
		};

		// if (Math.abs(this._depth) > this._radius * lensRadius) {
		// 	this._depthVelocity *= -1;
		// }
		// this._depth += this._depthVelocity;

		const gradient = [
			[30, 235, 160],
			[30, 70, 130]
		];

		for ( let i = 0; i < this._particleCount; i++ ) {

			// Apply lens

			let dot = {
				x: this._pointPositions[i * 3] + this._pointVelocities[i * 3],
				y: this._pointPositions[i * 3 + 1] + this._pointVelocities[i * 3 + 1],
				z: this._pointPositions[i * 3 + 2] + this._pointVelocities[i * 3 + 2]
			};

			const d = distance(mouse, dot);
			if (d < this._radius * lensRadius) {

				const f = d / this._radius;
				const mix = moveDistanceFromPoint(mouse, dot, this._radius * lensRadius);

				dot = {
					x: dot.x * f + mix.x * (1 - f),
					y: dot.y * f + mix.y * (1 - f),
					z: dot.z * f + mix.z * (1 - f)
				}

			}

			// Boundaries and physics

			if (distance(center, dot) > this._radius && this._radius > 0) {

				const reflectionPlane = reverse(dot);
				const velocity = point(this._pointVelocities[i * 3], this._pointVelocities[i * 3 + 1], this._pointVelocities[i * 3 + 2]);
				const relectedVel = reflect(velocity, reflectionPlane);

				this._pointVelocities[i * 3] = relectedVel.x;
				this._pointVelocities[i * 3 + 1] = relectedVel.y;
				this._pointVelocities[i * 3 + 2] = relectedVel.z;

				dot = moveDistanceFromPoint(center, dot, this._radius);

			}

			this._pointPositions[i * 3] = dot.x;
			this._pointPositions[i * 3 + 1] = dot.y;
			this._pointPositions[i * 3 + 2] = dot.z;

			this._pointVelocities[i * 3] += this._pointAcc[i * 3];
			this._pointVelocities[i * 3 + 1] += this._pointAcc[i * 3 + 1];
			this._pointVelocities[i * 3 + 2] += this._pointAcc[i * 3 + 2];

			{
				const f = this._radius ? (dot.z + this._radius) / 2 / this._radius : 0;
				const color = Color.gradient(gradient, bounds(0, 1, f));

				this._pointColors[i * 3] = color[0] / 255;
				this._pointColors[i * 3 + 1] = color[1] / 255;
				this._pointColors[i * 3 + 2] = color[2] / 255;
				this._pointScales[i] = f * .5 + .75;

			}

		}

		this._cloudGeometry.attributes.position.needsUpdate = true;
		this._cloudGeometry.attributes.color.needsUpdate = true;
		this._cloudGeometry.attributes.scale.needsUpdate = true;

	}

	_render() {
		this.renderer.render( this.scene, this.camera );
	}

	_setup() {

		super._setup();

		this._particleCount = Math.round((this.size[0] / window.devicePixelRatio / 1000) * (this.size[1] / window.devicePixelRatio / 1000) * 2500) + 4000;

		this._cloudGeometry = this._generatePointCloudGeometry();
		this._cloudMesh = new THREE.Points( this._cloudGeometry, this.shaderMaterial );

		this.scene.add( this._cloudMesh );
		this.camera = this.perspectiveCamera;

		this.scene.add(this.camera);

	}

	_generatePointCloudGeometry() {

		const geometry = new THREE.BufferGeometry();

		this._pointPositions = new Float32Array( this._particleCount * 3 );
		this._pointVelocities = new Float32Array( this._particleCount * 3 );
		this._pointAcc = new Float32Array( this._particleCount * 3 );
		this._pointColors = new Float32Array( this._particleCount * 3 );
		this._pointScales = new Float32Array( this._particleCount );

		for ( var i = 0; i < this._particleCount; i++ ) {

			this._pointPositions[ i * 3     ] = 0; // x
			this._pointPositions[ i * 3 + 1 ] = 0; // y
			this._pointPositions[ i * 3 + 2 ] = 0; // z

			const vel = unit(point(-.5 + Math.random(), -.5 + Math.random(), -.5 + Math.random()));

			this._pointVelocities[ i * 3     ] = vel.x; // x
			this._pointVelocities[ i * 3 + 1 ] = vel.y; // y
			this._pointVelocities[ i * 3 + 2 ] = vel.z; // z

			this._pointAcc[ i * 3     ] = vel.x / 100 + vel.y / 1000; // x
			this._pointAcc[ i * 3 + 1 ] = vel.y / 100 + vel.z / 1000; // y
			this._pointAcc[ i * 3 + 2 ] = vel.z / 100 + vel.x / 1000; // z

		}

		geometry.setDrawRange( 0, this._particleCount );
		geometry.addAttribute( 'position', new THREE.BufferAttribute( this._pointPositions, 3 ).setDynamic( true ) );
		geometry.addAttribute( 'color', new THREE.BufferAttribute( this._pointColors, 3 ).setDynamic( true ) );
		geometry.addAttribute( 'scale', new THREE.BufferAttribute( this._pointScales, 1 ).setDynamic( true ) );

		return geometry;

	}

	get shaderMaterial() {

		const image = 'data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSI4IiBoZWlnaHQ9IjgiIHZpZXdCb3g9IjAgMCA4IDgiPjxjaXJjbGUgZmlsbD0iIzQ0QjQ5MSIgY3g9IjQiIGN5PSI0IiByPSI0Ii8+PC9zdmc+';

		return new THREE.ShaderMaterial( {

			uniforms: {
				texture: { type: 't', value: new THREE.TextureLoader().load( image ) },
				size: { type: 'f', value: 6 * window.devicePixelRatio }
			},
			vertexShader: this.vertexShader,
			fragmentShader: this.fragmentShader,
			transparent: true

		});

	}

	get fragmentShader() {

		return `
		
		uniform sampler2D texture;
		
		varying vec3 vColor;
		
		void main() {
			
			vec4 tex = texture2D( texture, gl_PointCoord );
			
			if ( tex.a < 0.5 ) discard;
			
			gl_FragColor = vec4( vColor, 1 );
			
		}`;

	}

	get vertexShader() {

		return `
		
		uniform float size;
		
		attribute vec3 color;
		
		attribute float scale;
		
		varying vec3 vColor;
		
		void main() {
			
			vColor = color;
			
			vec4 mvPosition = modelViewMatrix * vec4( position, 1.0 );
			
			gl_PointSize = size * scale;
			
			gl_Position = projectionMatrix * mvPosition;
			
		}`;

	}


}

export default Cloud;