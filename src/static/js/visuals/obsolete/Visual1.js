import Bounds from 'visuals/Bounds';
import observer from 'util/observer';
import measures from 'util/measures';
import ticker from 'motion/ticker';
import Color from 'util/Color';
import { canvas } from 'util/canvasHelpers';

const r = window.devicePixelRatio;

class Visual1 {

	constructor(element) {

		this._rotation = 0;
		this._phaseX = /*(measures.mouseX / measures.width -.5) * */50;
		this._phaseY = /*(measures.mouseY / measures.height -.5) * */22;
		this._amp = measures.mouseY / measures.height * 100;

		Object.assign(this, canvas(element)); 

		this._bounds = new Bounds();

		setTimeout(() => this._bounds.animateIn(), 1000);
		setTimeout(() => this._bounds.animateOut(), 10000);

		this._bindEvents();

	}

	_bindEvents() {
		observer.subscribe(ticker, 'tick', d => this._tick(d));
	}

	_tick(d) {

		this._phaseX = (this._phaseX * 14 + /*(measures.mouseX / measures.width -.5) * */50) / 15;
		this._phaseY = (this._phaseY * 14 + /*(measures.mouseY / measures.height -.5) * */22) / 15;
		this._amp = (this._amp * 14 + measures.mouseY / measures.height * 100) / 15;

		this._rotation += Math.PI * 2 / 360 * 90 * d;
		this._color = Color.gradient(['42ace3', 'e34268', '8fdf54', 'f4d52a', 'e34268', '8fdf54', '42ace3'], this._rotation / 40);
		this._ctx.rect(0, 0, measures.width * r, measures.height * 1.5 * r);
		this._ctx.fillStyle = 'rgba(255,255,255,0.1)';
		this._ctx.fill();
		this._drawCircle(350, this._rotation);
	}

	_drawCircle(segments, rotation = 0) {

		let da = Math.PI * 2 / segments,
			a = rotation,
			p = this._getModulatedPointInCircle(a, this._bounds.min, this._phaseX, this._phaseY, this._amp);

		this._ctx.beginPath();
		this._ctx.moveTo(p.x, p.y);

		while (a <= rotation + Math.PI * 2 * 6) {
			p = this._getModulatedPointInCircle(a, this._bounds.min + (a - rotation) * this._bounds.max, this._phaseX, this._phaseY, this._amp);
			a += da;
			this._ctx.lineTo(p.x, p.y);
		}

		this._ctx.strokeStyle = '#' + this._color;
		this._ctx.stroke();

	}

	_getPointInCircle(angle, radius) {

		return {
			x: measures.center.x * r + radius * r * Math.cos(angle),
			y: measures.center.y * r + radius * r * Math.sin(angle)
		};

	}

	_getModulatedPointInCircle(angle, radius, phaseX, phaseY, amp) {

		radius += .5 * amp * (Math.sin(angle * phaseX) + Math.cos(angle * phaseY)) / 2 - amp;

		return this._getPointInCircle(angle, radius);

	}

}

export default Visual1;