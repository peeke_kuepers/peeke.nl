import { makePlaybackObservable, makeNormalizedMouseObservable, Simulation, World } from 'visuals/helpers'
import interpolate from 'motion/interpolate'
import THREE from 'proxy/three'

const TAU = Math.PI * 2
const mod = (n, m) => (n + m) % m
const easeInOutQuart = t => t<.5 ? 8*t*t*t*t : 1-8*(--t)*t*t*t
const lerp = (value1, value2, amount) => {
	amount = amount < 0 ? 0 : amount;
	amount = amount > 1 ? 1 : amount;
	return value1 + (value2 - value1) * amount;
};

export default class SpringSurface {

  constructor (element) {

    this._resolution = 360
    this._friction = 1
    this._springForce = 133
    this._surfaceForce = 500
    this._cohesion = 2
    this._diffusion = 10
    this._previousGrab = null

    this._world = new World(element)
    this._radius = this._world.radius
    this._radiusSq = this._radius ** 2
    this._setup()

    this._segmentWidth = TAU * this._radius / this._surface.length

    this._simulation = new Simulation(dt => this._update(dt), 8)
    const playbackObservable = makePlaybackObservable(element)
    const normalizedMouseObservable = makeNormalizedMouseObservable(element)

    playbackObservable.forEach(state => {
      state === 'PLAY' && this._simulation.play()
      state === 'PAUSE' && this._simulation.pause()
    })

    normalizedMouseObservable.forEach(mouse => {
      mouse.x /= window.devicePixelRatio
      mouse.y /= window.devicePixelRatio
      this._grab(this._surface, mouse)
      this._mouse = mouse
    })

    this._simulation.play()
    this._render()

  }

  _setup () {
    this._sphere = this._createCirclePlane(this._resolution + 1)
    this._surface = this._sphere.geometry.vertices.slice(1)
    this._initSurface(this._surface)
    this._world.scene.add(this._sphere)
  }

  _update (dt) {

    let i = this._cohesion
    while (i--) {
      this._surface.forEach(vertice => {
        this._applySurfaceForce(vertice, vertice.cw, dt)
        this._applySurfaceForce(vertice, vertice.ccw, dt)
        this._diffuse(vertice, dt)
      })
    }

    this._surface
      .forEach(vertice => {
        this._applySpringForce(vertice, dt)
        this._applyFriction(vertice, dt)
        this._applyVelocity(vertice, dt)
      })

    this._volumeCorrect(this._surface)

    this._mouse && this._grab(this._surface, this._mouse)

    this._sphere.geometry.verticesNeedUpdate = true
    this._world.render()

  }

  _render () {
    if (this._simulation.isRunning) {
      this._world.render()
    }
    requestAnimationFrame(() => this._render())
  }

  _applySpringForce (vertice, dt) {
    const fBase = vertice.base.clone().sub(vertice)
    vertice.v.add(fBase.multiplyScalar(dt * this._springForce))
  }

  _applySurfaceForce (vertice, actor, dt) {
    const f = actor.clone().sub(vertice).divideScalar(this._segmentWidth)
    vertice.v.add(f.multiplyScalar(dt * this._surfaceForce))
  }

  _applyFriction (vertice, dt) {
    vertice.v.multiplyScalar(1 - this._friction * dt)
  }

  _diffuse (vertice, dt) {

    const p = vertice.clone()
      .multiplyScalar(1 - this._diffusion * dt)
      .add(vertice.cw.clone().multiplyScalar(this._diffusion * dt / 2))
      .add(vertice.ccw.clone().multiplyScalar(this._diffusion * dt / 2))

    vertice.copy(p)

  }

  _applyVelocity (vertice, dt) {
    // Project onto unit vector
    const p = new THREE.Vector2(vertice.x, vertice.y).add(vertice.v.clone().multiplyScalar(dt))
    const dot = p.dot(vertice.normal)
    vertice.set(vertice.normal.x * dot, vertice.normal.y * dot, 0)
  }

  _volumeCorrect (surface) {
    const segments = surface.length
    const idealVolume = Math.PI * this._radiusSq
    const currentVolume = surface.reduce((volume, vertice) => {
      const length = vertice.length()
      const segmentWidth = TAU * length / segments
      return volume + segmentWidth * length / 2
    }, 0)
    const correction = (4 + 1 / currentVolume * idealVolume) / 5
    surface.forEach(vertice => vertice.multiplyScalar(correction))
  }

  _grab (surface, point) {

    // if (Math.abs(point.x) < this._radius / 20 && Math.abs(point.y) < this._radius / 20) return

    const pointSq = point.x ** 2 + point.y ** 2
    if (pointSq > this._radiusSq) return

    point = new THREE.Vector2(point.x, point.y)
    if (point.length() < this._radius * .66) {
      point.normalize().multiplyScalar(this._radius * .66)
    }

    const a = (Math.atan2(point.y, point.x) / Math.PI * 180 + 360) % 360
    const na = Math.round(a / 360 * (surface.length - 1))

    if (surface[na].lengthSq() < pointSq) return

    // this._grabTimeout && clearTimeout(this._grabTimeout)
    // this._grabTimeout = setTimeout(() => this._previousGrab = null, 100)
    
    const length = point.length()

    for (let i = na - 10; i < na + 10; i++) {
      const n = mod(i, this._surface.length)
      const vertice = surface[n]
      const ownLength = vertice.length()
      const f = Math.abs(i - na) / 10
      vertice.normalize().multiplyScalar(lerp(length, lerp(this._radius, ownLength, f), easeInOutQuart(f)))
      vertice.v.x *= 1 - f
      vertice.v.y *= 1 - f
    }

    // const vertice = surface[n]
    // vertice.x = point.x
    // vertice.y = point.y
    // vertice.v.x = 0
    // vertice.v.y = 0

    // let start = this._previousGrab ? this._previousGrab.a : na
    // let end = na
    // if (end - start > surface.length / 2) {
    //   start += surface.length
    // } else if (start - end > surface.length / 2) {
    //   end += surface.length
    // }

    // const prevX = this._previousGrab ? this._previousGrab.x : point.x
    // const prevY = this._previousGrab ? this._previousGrab.y : point.y

    // interpolate(start, end)
    //   .map(n => n % surface.length)
    //   .forEach((n, i, arr) => {
    //     const vertice = surface[n]
    //     vertice.x = prevX + (point.x - prevX) / arr.length * i
    //     vertice.y = prevY + (point.y - prevY) / arr.length * i
    //     vertice.v.x = 0
    //     vertice.v.y = 0
    //   })

    this._previousGrab = {
      a: na,
      x: point.x,
      y: point.y
    }
  }

  _createCirclePlane (resolution) {

    const geometry = new THREE.Geometry()
    geometry.dynamic = true
    geometry.vertices = Array(resolution)
      .fill(null)
      .map((_, i) => {
        if (i === 0) return new THREE.Vector3(0, 0, 0)
        const a = (i - 1) / resolution * TAU
        return new THREE.Vector3(
          Math.cos(a) * this._radius,
          Math.sin(a) * this._radius,
          0
        )
      })

    geometry.vertices.forEach((vertice, i) => {
      const j = i === geometry.vertices.length - 1 ? 1 : i + 1
      geometry.faces.push(new THREE.Face3(0, i, j))
    })

    geometry.verticesNeedUpdate = true

    const material = new THREE.MeshBasicMaterial({color: 0x12d5a5})
    return new THREE.Mesh(geometry, material)

  }

  _initSurface (surface) {
    surface.forEach((vertice, i) => {
      vertice.base = new THREE.Vector2(vertice.x, vertice.y)
      vertice.normal = vertice.base.clone().normalize()
      vertice.v = new THREE.Vector2(0, 0)
      vertice.cw = surface[mod(i + 1, surface.length)]
      vertice.ccw = surface[mod(i - 1, surface.length)]
    })
  }

}