import THREE from 'proxy/three'

import setupGPUComputationRenderer from "three/GPUComputationRenderer";

const GPUComputationRenderer = setupGPUComputationRenderer(THREE)

import TexturePass from "three/TexturePass";

import { compute, fragment } from 'visuals/water/shaders'

const envPx = "/textures/dpdk/px.jpg";
const envPy = "/textures/dpdk/py.jpg";
const envPz = "/textures/dpdk/py.jpg";
const envNx = "/textures/dpdk/nx.jpg";
const envNz = "/textures/dpdk/nz.jpg";
const bottom = "/textures/dpdk/bottom.jpg";
const stencil = "/textures/dpdk/stencil.png";
const white = "/textures/dpdk/white.png";
const waveMaskTexture = "/textures/dpdk/mask.png";

import { makePlaybackObservable } from 'visuals/helpers'
let frame

function visual() {
  const rad = deg => (deg / 180) * Math.PI;
  const calculateViewportHeight = (perspectiveAngle, distance) => {
    return Math.tan(rad(perspectiveAngle / 2)) * distance * 2;
  };

  const width = Math.round(Math.min(window.innerWidth, window.innerHeight) * .67)
  const height = Math.round(Math.min(window.innerWidth, window.innerHeight) * .67);
  const canvas = document.createElement('canvas')
  canvas.width = width
  canvas.height = height
  canvas.style.width = width + 'px'
  canvas.style.height = height + 'px'
  canvas.style.borderRadius = '50%'

  console.log(width, height)
  
  const viewportHeight = calculateViewportHeight(75, 30);

  const topScene = new THREE.Scene();
  const bottomScene = new THREE.Scene();

  const camera = new THREE.OrthographicCamera(
    viewportHeight / -2,
    viewportHeight / 2,
    viewportHeight / 2,
    viewportHeight / -2,
    1,
    1000
  );
  camera.position.z = 30;

  const intermediateTarget = new THREE.WebGLRenderTarget(width, height);
  const renderer = new THREE.WebGLRenderer({
    canvas,
    alpha: true
  });
  // renderer.setPixelRatio(window.devicePixelRatio);
  renderer.setSize(width, height);
  renderer.setClearColor(0xffffff);

  const normalMapPass = new TexturePass(renderer, fragment.normalMap);
  const causticsPass = new TexturePass(renderer, fragment.caustics, {
    u_intensity: { value: .2 }
  });
  const refractPass = new TexturePass(renderer, fragment.refract, {
    u_refract: {
      value: new THREE.Texture()
    }
  });
  const combinePass = new TexturePass(renderer, fragment.screen, {
    u_texture2: {
      value: new THREE.Texture()
    }
  });

  const planeGeometry = new THREE.PlaneGeometry(
    (viewportHeight * width) / height,
    viewportHeight,
    32
  );

  const envMap = new THREE.CubeTextureLoader().load([
    envPx,
    envNx,
    envPy,
    white,
    envPz,
    envNz
  ]);

  const stencilMap = new THREE.TextureLoader().load(stencil);
  const bottomTexture = new THREE.TextureLoader().load(bottom);

  const bottomPlaneMaterial = new THREE.MeshBasicMaterial({
    color: 0xffffff,
    map: bottomTexture
  });
  const bottomPlane = new THREE.Mesh(planeGeometry, bottomPlaneMaterial);
  bottomPlane.receiveShadow = true;
  bottomPlane.position.set(0, 0, -10);
  bottomScene.add(bottomPlane);

  const bottomFlattenedPlaneMaterial = new THREE.MeshBasicMaterial({
    color: 0xffffff
  });
  const bottomFlattenedPlane = new THREE.Mesh(
    planeGeometry,
    bottomFlattenedPlaneMaterial
  );
  bottomFlattenedPlane.position.set(0, 0, -10);
  topScene.add(bottomFlattenedPlane);

  const topPlaneMaterial = new THREE.MeshPhongMaterial({
    color: 0xd1faf2,
    premultipliedAlpha: true,
    transparent: true,
    opacity: 0.45,
    shininess: 100,
    specular: 0x18588e,
    envMap,
    combine: THREE.MixOperation,
    reflectivity: 0.8
  });
  const topPlane = new THREE.Mesh(planeGeometry, topPlaneMaterial);
  topPlane.position.set(0, 0, 0);
  topScene.add(topPlane);

  const stencilPlaneMaterial = new THREE.MeshBasicMaterial({
    color: 0xffffff,
    transparent: true,
    alphaMap: stencilMap
  });
  const stencilPlane = new THREE.Mesh(planeGeometry, stencilPlaneMaterial);
  stencilPlane.position.set(0, 0, 0);
  topScene.add(stencilPlane);

  const light = new THREE.DirectionalLight(0xffffff, 1);
  light.position.set(viewportHeight * 2, viewportHeight * 2, viewportHeight);
  light.lookAt(0, 0, 0);
  light.castShadow = true;
  bottomScene.add(light.clone());
  topScene.add(light);

  const ambientLight = new THREE.AmbientLight(0xffffff, 0.5); // soft white light
  bottomScene.add(ambientLight.clone());
  topScene.add(ambientLight);

  const waveMask = new THREE.TextureLoader().load(waveMaskTexture)
  waveMask.magFilter = THREE.NearestFilter;
  waveMask.minFilter = THREE.NearestFilter;

  const gpuCompute = new GPUComputationRenderer(width, height, renderer);
  const gpTexture = gpuCompute.createTexture();
  initTexture(gpTexture);

  const gpVariable = gpuCompute.addVariable(
    "textureWaves",
    compute.waves,
    gpTexture
  );
  gpuCompute.setVariableDependencies(gpVariable, [gpVariable]);

  gpVariable.material.uniforms = {
    u_inv_resolution: {
      value: new THREE.Vector2(1 / width, 1 / height)
    },
    u_mouse: {
      value: new THREE.Vector2(-1, -1)
    },
    u_waveMask: {
      value: waveMask
    }
  };

  let mouse = new THREE.Vector2(-1, -1)
  let offset = canvas.getBoundingClientRect();
  let offsetTimeout
  
  window.addEventListener("mousemove", e => {
    if (!offsetTimeout) {
      offset = canvas.getBoundingClientRect();
      clearTimeout(offsetTimeout)
      offsetTimeout = setTimeout(() => {
        offsetTimeout = null
      }, 800)
    }
    mouse = new THREE.Vector2(e.clientX - offset.left, height - e.clientY + offset.top);
  });

  const error = gpuCompute.init();
  if (error !== null) {
    throw error;
  }

  function initTexture(texture) {
    const pixels = texture.image.data;
    for (let i = 0; i < pixels.length; i += 4) {
      pixels[i] = 0.5;
      pixels[i + 1] = 0.5;
      pixels[i + 2] = 0.5;
      pixels[i + 3] = 0.5;
    }
  }

  const render = () => {

    // Set uniforms: mouse interaction
    gpVariable.material.uniforms.u_mouse.value = mouse;

    // Do the gpu computation
    let i = 3;
    while (i--) {
      gpuCompute.compute();
    }

    // Get compute output in custom uniform
    const renderTarget = gpuCompute.getCurrentRenderTarget(gpVariable);
    const heightMap = renderTarget.texture;
    const normalMap = normalMapPass.process(heightMap);

    const caustics = causticsPass.process(normalMap);

    combinePass.uniforms.u_texture2.value = bottomTexture;
    const combined = combinePass.process(caustics);

    bottomPlaneMaterial.map = combined;
    // bottomPlaneMaterial.map = caustics;
    topPlaneMaterial.normalMap = normalMap;

    renderer.render(bottomScene, camera, intermediateTarget);

    // bottomFlattenedPlaneMaterial.map = intermediateTarget.texture;
    refractPass.uniforms.u_refract.value = heightMap;
    bottomFlattenedPlaneMaterial.map = refractPass.process(
      intermediateTarget.texture
    );

    renderer.render(topScene, camera);

    if (Math.random() < .1) {
      mouse = new THREE.Vector2(Math.random() * width, Math.random() * height);
    } else {
      mouse = new THREE.Vector2(-1, -1);  
    }
  };
  
  const loop = () => {
    render();
    frame = requestAnimationFrame(loop);
  }
  
  const start = () => {
    loop();
  }
  
  const pause = () => {
    cancelAnimationFrame(frame);
  }

  return {
    start, 
    pause,
    canvas
  }
}

export default class Controller {
  constructor(container, options = {}) {
    const {
      start, 
      pause,
      canvas
    } = visual()

    const playbackObservable = makePlaybackObservable(container)

    playbackObservable.forEach(state => {
      state === 'PLAY' && start()
      state === 'PAUSE' && pause()
    })
    
    container.appendChild(canvas)
    start()

  }
}