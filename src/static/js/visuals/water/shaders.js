const waves = `
uniform vec2 u_mouse;
uniform vec2 u_inv_resolution;
uniform sampler2D u_waveMask;

vec4 s(vec2 p){
  vec2 uv=p*u_inv_resolution;
  return texture2D(textureWaves,uv);
}

vec4 s(vec2 p,vec2 o){
  vec2 uv=p*u_inv_resolution;
  vec2 no=o*u_inv_resolution;
  
  if(texture2D(u_waveMask,uv+no).x<1.){
    return texture2D(textureWaves,uv);
  }
  
  return texture2D(textureWaves,uv+no);
}

float dot(vec2 v){
  return dot(v,v);
}

float lerp(float a,float b,float t){
  return a+clamp(t,0.,1.)*(b-a);
}

void main(){
  
  vec2 uv=gl_FragCoord.xy*u_inv_resolution.xy;
  vec2 t=gl_FragCoord.xy;
  
  // https://web.archive.org/web/20160418004149/http://freespace.virgin.net/hugo.elias/graphics/x_water.htm
  // Buffer1 = x
  // Buffer2 = y
  // Alpha channel = w
  
  vec2 ox=vec2(1.,.0);// offsetX
  vec2 oy=vec2(.0,1.);// offsetY
  
  float color=(s(t,ox).x+s(t,-ox).x+s(t,oy).x+s(t,-oy).x)/2.-s(t).y;
  
  float dist=length(u_mouse-t.xy);
  if(dist<40.){
    float t=1.-dist/40.;
    float eased=(1.+sin(3.1415*t-3.1415/2.))/2.;
    color=lerp(color,color+.00025,eased);
  }
  
  color=color*.999+(s(t,ox).x+s(t,-ox).x+s(t,oy).x+s(t,-oy).x)/4.*.001;
  
  float newColor=clamp(color*.9995+.5*.0005,0.,1.);
  float oldColor=s(t).x;
  
  gl_FragColor=vec4(newColor,oldColor,1.,1.);
}
`

export const compute = { waves }

const caustics = `
uniform vec2 u_inv_resolution;
uniform sampler2D u_texture;
uniform float u_intensity;

#define REFRACTION 1.33
#define LIGHT normalize(vec3(1.,1.,1.))
#define DEPTH 10.
// #define HALF_SUNDISK .004884

float caustic(vec2 uv){
  vec4 normal=(texture2D(u_texture,uv)-.5)*2.;
  float angle=acos(dot(normal.xyz,LIGHT));
  // return angle<1.23?1.:0.;
  return 1.-clamp(angle*REFRACTION,0.,1.25)/1.2;
}

void main(){
  vec2 d=LIGHT.xy/LIGHT.z*DEPTH;
  vec2 uv=(gl_FragCoord.xy+d)*u_inv_resolution.xy;
  
  float color=caustic(uv);
  
  gl_FragColor=vec4(vec3(color*u_intensity),1.);
}
`

const normalMap = `
uniform vec2 u_inv_resolution;
uniform sampler2D u_texture;

void main(){
  
  vec2 uv=gl_FragCoord.xy*u_inv_resolution.xy;
  float val=texture2D(u_texture,uv).x;
  
  float val_u=texture2D(u_texture,uv+vec2(u_inv_resolution.x,0.)).x;
  float val_v=texture2D(u_texture,uv+vec2(0.,u_inv_resolution.y)).x;
  
  float height=.0025;
  gl_FragColor=vec4((.5*normalize(vec3(val-val_u,val-val_v,height))+.5),1.);
  
}
`

const refract = `
uniform vec2 u_inv_resolution;
uniform sampler2D u_texture;
uniform sampler2D u_refract;

void main(){
  vec2 uv=gl_FragCoord.xy*u_inv_resolution.xy;
  vec2 d=texture2D(u_refract,uv).xy*u_inv_resolution.xy-u_inv_resolution.xy*.5;
  vec4 sample=texture2D(u_texture,uv+d*200.);
  
  gl_FragColor=sample;
}
`

const screen = `
uniform vec2 u_inv_resolution;
uniform sampler2D u_texture;
uniform sampler2D u_texture2;

void main(){
  vec2 uv=gl_FragCoord.xy*u_inv_resolution.xy;
  vec3 base=texture2D(u_texture,uv).xyz;
  vec3 blend=texture2D(u_texture2,uv).xyz;
  vec3 sample=min(base+blend,vec3(1.));
  gl_FragColor=vec4(sample,1.);
}
`

export const fragment = { caustics, normalMap, refract, screen }

const basic = `
void main() {
  gl_Position = projectionMatrix * modelViewMatrix * vec4(position, 1.0);
}
`

export const vertex = { basic }