import VisualBase from 'visuals/VisualBase'
import animate from 'motion/animate'
import { easeInOutQuart } from 'motion/eases'
import MatrixIndex from 'util/MatrixIndex'

import {
  abs,
  diff,
  distance,
  point,
  multiply,
  moveDistanceFromPoint,
  bounds,
  reflect,
  reverse,
  unit
} from 'math/mathHelpers'
import measures from 'util/measures'
import THREE from 'proxy/three'

const MAX_PARTICLES = 2000
const DENSITY = window.devicePixelRatio

class Blob extends VisualBase {

  constructor (element) {

    super(element)

    this._scale = .1

    animate(0, 1, easeInOutQuart, 1000, r => this._scale = r)

    this.reveal()

  }

  reveal () {
    this.run()
  }

  hide () {
    animate(1, 0, easeInOutQuart, 1000, r => this._scale = r)
      .then(() => this._destroy())
  }

  // dt = deltaTime (s)
  _simulate (dt) {

    if (!this._radius) return

    const center = point(0, 0, 0)
    const [width, height] = this.size
    const mouse = {
      x: (measures.mouse.x - width / 2 / DENSITY) * DENSITY,
      y: (height / 2 / DENSITY - measures.mouse.y) * DENSITY
    }

    for (let i = 0; i < this._particleCount; i++) {

      let point = {
        x: this._pointPositions[i * 2] + this._pointVelocities[i * 2],
        y: this._pointPositions[i * 2 + 1] + this._pointVelocities[i * 2 + 1]
      }

      let velocity = {
        x: this._pointVelocities[i * 2],
        y: this._pointVelocities[i * 2 + 1]
      }

      const dc = diff(point, center)
      const dm = diff(point, mouse)
      const possiblyOutside = abs(dc.x) > this._cullRadius || abs(dc.y) > this._cullRadius

      if (possiblyOutside) {

        const dist = distance(point, center)

        if (dist > this._radius) {
          point = multiply(unit(point), this._radius)
          velocity = reflect(velocity, reverse(point))
        }

      }

      this._pointPositions[i * 2] = point.x
      this._pointPositions[i * 2 + 1] = point.y

      this._matrixIndex.update(this._pointPositions, this._radius * 2, 10)

      const gravity = multiply(unit(dc), -.01)
      const normalizedDistance = Math.min(this._radius * 2, Math.max(abs(dm.x, dm.y))) / this._radius / 2
      const mouseRepulsion = multiply(unit(dm), .25 * (1 - normalizedDistance))

      this._pointVelocities[i * 2] = (velocity.x + gravity.x + mouseRepulsion.x) * .99
      this._pointVelocities[i * 2 + 1] = (velocity.y + gravity.y + mouseRepulsion.y) * .99

      // const f = this._radius ? (point.z + this._radius) / 2 / this._radius : 0
      const color = [55, 55, 55]

      this._pointColors[i * 3] = color[0] / 255
      this._pointColors[i * 3 + 1] = color[1] / 255
      this._pointColors[i * 3 + 2] = color[2] / 255
      // this._pointScales[i] = f * .5 + .75

      this._displayPositions[i * 3] = this._pointPositions[i * 2] // x
      this._displayPositions[i * 3 + 1] = this._pointPositions[i * 2 + 1] // y
      this._displayPositions[i * 3 + 2] = 0 // z

    }

    this._cloudGeometry.attributes.position.needsUpdate = true
    // this._cloudGeometry.attributes.color.needsUpdate = true
    // this._cloudGeometry.attributes.scale.needsUpdate = true

  }

  _render () {
    this.renderer.render(this.scene, this.camera)
  }

  _setup () {

    super._setup()

    this._particleCount = MAX_PARTICLES //Math.round((this.size[0] / DENSITY / 1000) * (this.size[1] / DENSITY / 1000) * 2500) + 4000
    this._radius = this.vmin / 3
    this._cullRadius = this._radius * .577
    this._matrixIndex = new MatrixIndex(this._particleCount)

    this._cloudGeometry = this._generatePointCloudGeometry()
    this._cloudMesh = new THREE.Points(this._cloudGeometry, this.shaderMaterial)

    this.scene.add(this._cloudMesh)
    this.camera = this.perspectiveCamera

    this.scene.add(this.camera)

  }

  _generatePointCloudGeometry () {

    const geometry = new THREE.BufferGeometry()

    this._pointPositions = new Float32Array(this._particleCount * 2)
    this._displayPositions = new Float32Array(this._particleCount * 3)
    this._pointVelocities = new Float32Array(this._particleCount * 2)
    this._pointAcc = new Float32Array(this._particleCount * 2)
    this._pointColors = new Float32Array(this._particleCount * 3)
    this._pointScales = new Float32Array(this._particleCount)

    for (let i = 0; i < this._particleCount; i++) {

      this._pointPositions[i * 2] = 2 * this._radius * (Math.random() - .5) // x
      this._pointPositions[i * 2 + 1] = 2 * this._radius * (Math.random() - .5) // y

      this._displayPositions[i * 3] = this._pointPositions[i * 2] // x
      this._displayPositions[i * 3 + 1] = this._pointPositions[i * 2 + 1] // y
      this._displayPositions[i * 3 + 2] = 0 // z

      this._pointVelocities[i * 2] = -.5 + Math.random() // x
      this._pointVelocities[i * 2 + 1] = -.5 + Math.random() // y
      this._pointVelocities[i * 2 + 2] = -.5 + Math.random() // z

      this._pointScales[i] = 1

    }

    geometry.setDrawRange(0, this._particleCount)
    geometry.addAttribute('position', new THREE.BufferAttribute(this._displayPositions, 3).setDynamic(true))
    geometry.addAttribute('color', new THREE.BufferAttribute(this._pointColors, 3).setDynamic(true))
    geometry.addAttribute('scale', new THREE.BufferAttribute(this._pointScales, 1).setDynamic(true))

    return geometry

  }

  get shaderMaterial () {

    const image = 'data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSI4IiBoZWlnaHQ9IjgiIHZpZXdCb3g9IjAgMCA4IDgiPjxjaXJjbGUgZmlsbD0iIzQ0QjQ5MSIgY3g9IjQiIGN5PSI0IiByPSI0Ii8+PC9zdmc+'

    return new THREE.ShaderMaterial({

      uniforms: {
        texture: {type: 't', value: new THREE.TextureLoader().load(image)},
        size: {type: 'f', value: 6 * DENSITY}
      },
      vertexShader: this.vertexShader,
      fragmentShader: this.fragmentShader,
      transparent: true

    })

  }

  get fragmentShader () {

    return `
		
		uniform sampler2D texture;
		
		varying vec3 vColor;
		
		void main() {
			
			vec4 tex = texture2D( texture, gl_PointCoord );
			
			if ( tex.a < 0.5 ) discard;
			
			gl_FragColor = vec4( vColor, 1 );
			
		}`

  }

  get vertexShader () {

    return `
		
		uniform float size;
		
		attribute vec3 color;
		
		attribute float scale;
		
		varying vec3 vColor;
		
		void main() {
			
			vColor = color;
			
			vec4 mvPosition = modelViewMatrix * vec4( position, 1.0 );
			
			gl_PointSize = size * scale;
			
			gl_Position = projectionMatrix * mvPosition;
			
		}`

  }

}

export default Blob