import 'vendor/matter/matter'
import MetaBalls from 'motion/MetaBalls'
import { unit, multiply, diff } from 'math/mathHelpers'
import { debounceTail } from 'util/eventHelpers'
import { makePlaybackObservable, makeNormalizedMouseObservable, Simulation } from 'visuals/helpers'

const {Engine, World, Bodies, Body} = Matter
const GROW = .15

class MetaBlob {

  constructor (container) {

    this._container = container

    this._size = {width: container.offsetWidth, height: container.offsetHeight}
    this._mouse = false

    this._canvas = document.createElement('canvas')
    this._canvas.width = this._size.width
    this._canvas.height = this._size.height
    this._container.appendChild(this._canvas)

    this._particleCount = 70
    this._metaBalls = new MetaBalls(this._canvas, {size: this._particleCount})

    this._engine = this.setupEngine()

    const playbackObservable = makePlaybackObservable(container)
    const normalizedMouseObservable = makeNormalizedMouseObservable(container, {inverted: false, centered: false, applyDensity: false})

    playbackObservable.forEach(state => {
      state === 'PLAY' ? this.play() : this.pause()
    })

    normalizedMouseObservable.forEach(mouse => {
      this._mouse = mouse
    })

    this.onResizeEnd = debounceTail(this.onResizeEnd.bind(this), 100)
    window.addEventListener('resize', this.onResizeEnd, {passive: true})

    this.play()

  }

  get radius () {
    return Math.min(this._size.width, this._size.height) * .333 / (1 + GROW / 2)
  }

  get center () {
    return {
      x: this._size.width / 2,
      y: this._size.height / 2
    }
  }

  get particleSize () {
    return Math.round(this.radius * .058)
  }

  play() {
    this._engine.enabled = true
    this._render()
  }

  pause() {
    this._engine.enabled = false
  }

  setupEngine () {

    // create an engine
    const engine = Engine.create({
      constraintIterations: 1,
      positionIterations: 1,
      velocityIterations: 1,
      // enableSleeping: true
    })

    engine.world.gravity.y = 0

    // create two boxes and a ground
    this._mouseParticle = this.createParticles(1, 6.5, 1).pop()
    this._particles = this.createParticles(this._particleCount)
    this._ring = this.createRing()

    // add all of the bodies to the world
    World.add(engine.world, this._ring)
    World.add(engine.world, this._particles)
    World.add(engine.world, this._mouseParticle)

    // run the engine
    Engine.run(engine)

    return engine

  }

  onResizeEnd () {

    this._size = {
      width: this._container.offsetWidth,
      height: this._container.offsetHeight
    }

    this._canvas.width = this._size.width
    this._canvas.height = this._size.height

    this._particles
      .map((_, i) => this.positionParticle(this._particles.length, i))
      .forEach((pos, i) => Body.setPosition(this._particles[i], pos))

    World.remove(this._engine.world, this._ring)
    this._ring = this.createRing()
    World.add(this._engine.world, this._ring)

  }

  createRing () {

    const segs = 16
    const stepAngle = Math.PI * 2 / segs

    const parts = new Array(segs)
      .fill(null)
      .map((_, i) => stepAngle * i)
      .map(rad => Bodies.rectangle(
        Math.cos(rad) * (this.radius + 300),
        Math.sin(rad) * (this.radius + 300),
        600,
        600,
        {angle: rad}
      ))

    const ring = Body.create({parts, isStatic: true})
    Body.setPosition(ring, this.center)

    return ring

  }

  createParticles (n, size = 1, density = .0001) {
    const opts = () => ({density, restitution: 1, friction: 0.00001, scale: .25 + Math.random() * 1.5})
    return new Array(n)
      .fill(null)
      .map((_, i) => this.positionParticle(n, i))
      .map(({x, y}) => Bodies.circle(x, y, this.particleSize * size, opts()))
  }

  positionParticle (n, i) {
    const a = 6 * Math.PI / n * i
    const r = (i / n * .8 + .2) * (this.radius - this.particleSize / 2)
    return {
      x: this.center.x + Math.cos(a) * r,
      y: this.center.y + Math.sin(a) * r
    }
  }

  _render() {

    if (!this._engine.enabled) return

    this._mouse && Body.setPosition(this._mouseParticle, this._mouse)

    this._metaBalls.render(
      this._particles.map(p => p.position.x),
      this._particles.map(p => p.position.y),
      this._particles.map(p => p.scale * this.particleSize * (1 + GROW))
    )

    requestAnimationFrame(() => this._render())

  }

}

export default MetaBlob