import { makePlaybackObservable, makeNormalizedMouseObservable, Simulation, World } from 'visuals/helpers'
import THREE from 'proxy/three'
import gradient from 'util/gradient'

const TAU = Math.PI * 2

export default class Synapses {

  constructor (element) {

    this._pointCount = 350
    this._maxConnections = 14
    this._mouseForce = 250
    this._gravity = 40
    this._friction = .05
    this._repellingForce = 40
    this._fastSq = 7 ** 2
    this._gradient = gradient([30, 15, 85], [238, 24, 100])

    this._world = new World(element)
    this._radius = this._world.radius
    this._radiusSq = this._radius ** 2
    this._minDistance = this._radius / 6
    this._minDistanceSq = this._minDistance ** 2
    this._setup()

    this._simulation = new Simulation(dt => this._update(dt), 16)
    const playbackObservable = makePlaybackObservable(element)
    const normalizedMouseObservable = makeNormalizedMouseObservable(element)

    playbackObservable.forEach(state => {
      state === 'PLAY' && this._simulation.play()
      state === 'PAUSE' && this._simulation.pause()
    })

    normalizedMouseObservable.forEach(mouse => {
      this._mouse = mouse
      this._mouse.x /= 2
      this._mouse.y /= 2
    })

    this._simulation.play()
    this._render()

  }

  _setup () {
    this._cloud = this._generateCloud(this._pointCount)
    this._world.scene.add(this._cloud)
  }

  _update (dt) {

    const connections = new Array(this._pointCount).fill(0)

    let drawRange = 0

    for (let i = 0; i < this._pointCount; i++) {

      this._applyMouseForce(i, dt)

      for (let j = i + 1; j < this._pointCount; j++) {

        if (connections[i] >= this._maxConnections || connections[j] >= this._maxConnections)
          continue

        const drawn = this._makeConnections(i, j, drawRange * 2, drawRange * 4, dt)
        if (!drawn) continue
        drawRange += 2

        connections[i]++
        connections[j]++

        this._applyRepellingForce(i, j, dt)

      }

      this._applyVelocity(i, dt)
      this._applyFriction(i)
      // this._applyGravity(i, dt)
      this._confine(i)

    }

    this._cloud.geometry.attributes.color.needsUpdate = true
    this._cloud.geometry.attributes.position.needsUpdate = true
    this._cloud.geometry.setDrawRange(0, drawRange)

  }

  _render() {
    if (this._simulation.isRunning) {
      this._world.render()
    }
    requestAnimationFrame(() => this._render())
  }

  _confine (i) {

    const lengthSq = this._particles[i * 2] ** 2 + this._particles[i * 2 + 1] ** 2
    if (lengthSq < this._radiusSq) return

    this._particles[i * 2] *= this._radiusSq / lengthSq
    this._particles[i * 2 + 1] *= this._radiusSq / lengthSq

    const normal = new THREE.Vector3(this._particles[i * 2], this._particles[i * 2 + 1], 0).normalize()
    const reflected = new THREE.Vector3(this._particleSpeeds[i * 2], this._particleSpeeds[i * 2 + 1], 0).reflect(normal)
    this._particleSpeeds[i * 2] = reflected.x
    this._particleSpeeds[i * 2 + 1] = reflected.y

  }

  _applyMouseForce(i, dt) {

    if (!this._mouse) return

    const dx = this._particles[i * 2] - this._mouse.x
    const dy = this._particles[i * 2 + 1] - this._mouse.y
    const f = this._getRepellingForce(dx, dy, this._mouseForce, this._radiusSq * 1.33)

    this._particleSpeeds[i * 2] += f.x * dt
    this._particleSpeeds[i * 2 + 1] += f.y * dt

  }

  _applyFriction(i) {
    this._particleSpeeds[i * 2] *= 1 - this._friction
    this._particleSpeeds[i * 2 + 1] *= 1 - this._friction
  }

  _applyVelocity (i, dt) {
    this._particles[i * 2] += this._particleSpeeds[i * 2] * dt
    this._particles[i * 2 + 1] += this._particleSpeeds[i * 2 + 1] * dt
  }

  _applyRepellingForce(i, j, dt) {

    const dx = this._particles[i * 2] - this._particles[j * 2]
    const dy = this._particles[i * 2 + 1] - this._particles[j * 2 + 1]
    const f = this._getRepellingForce(dx, dy, this._repellingForce, this._radiusSq / 60)

    this._particleSpeeds[i * 2] += f.x / 2 * dt
    this._particleSpeeds[i * 2 + 1] += f.y / 2 * dt
    this._particleSpeeds[j * 2] -= f.x / 2 * dt
    this._particleSpeeds[j * 2 + 1] -= f.y / 2 * dt

  }

  _applyGravity(i, dt) {
    this._particleSpeeds[i * 2] -= (this._particles[i * 2] / this._radius) * this._gravity * dt
    this._particleSpeeds[i * 2 + 1] -= (this._particles[i * 2 + 1] / this._radius) * this._gravity * dt
  }

  _getRepellingForce(dx, dy, force, maxSq) {
    const lSq = dx ** 2 + dy ** 2
    if (lSq > maxSq) return { x: 0, y: 0 }
    const r = force * (this._radiusSq - lSq) / lSq / this._radius
    return { x: dx * r, y: dy * r }
  }

  _makeConnections (i, j, vertexRange, colorRange) {

    const dx = this._particles[i * 2] - this._particles[j * 2]
    const dy = this._particles[i * 2 + 1] - this._particles[j * 2 + 1]
    const lengthSq = dx ** 2 + dy ** 2

    if (lengthSq < this._minDistanceSq) {

      const alpha = 1 - Math.min(1, (lengthSq + 50) / (this._minDistanceSq + 50))

      this._positions[vertexRange] = this._particles[j * 2]
      this._positions[vertexRange + 1] = this._particles[j * 2 + 1]
      this._positions[vertexRange + 2] = this._particles[i * 2]
      this._positions[vertexRange + 3] = this._particles[i * 2 + 1]

      const c1 = this._gradient(this._particleSpeeds[i * 2] * this._particleSpeeds[i * 2 + 1] / this._fastSq)
      const c2 = this._gradient(this._particleSpeeds[j * 2] * this._particleSpeeds[j * 2 + 1] / this._fastSq)
      this._colors[colorRange] = c1[0] / 255
      this._colors[colorRange + 1] = c1[1] / 255
      this._colors[colorRange + 2] = c1[2] / 255
      this._colors[colorRange + 3] = alpha
      this._colors[colorRange + 4] = c2[0] / 255
      this._colors[colorRange + 5] = c2[1] / 255
      this._colors[colorRange + 6] = c2[2] / 255
      this._colors[colorRange + 7] = alpha

      return true

    }

  }

  _generateCloud (pointCount) {

    const geometry = new THREE.BufferGeometry()
    const material = new THREE.RawShaderMaterial( {
      vertexShader: Synapses.vertexShader,
      fragmentShader: Synapses.fragmentShader,
      transparent: true
    } );

    this._particles = new Float32Array(pointCount * 2)
    this._particleSpeeds = new Float32Array(pointCount * 2)
    this._positions = new Float32Array(pointCount * 2 * this._maxConnections * 2).fill(0)
    this._colors = new Float32Array(pointCount * 4 * this._maxConnections * 2).fill(0)

    for (let i = 0; i < pointCount; i++) {

      this._particles[i * 2] = this._radius * 2 * Math.random() - this._radius
      this._particles[i * 2 + 1] = this._radius * 2 * Math.random() - this._radius

      const lengthSq =
        this._particles[i * 2] ** 2 +
        this._particles[i * 2 + 1] ** 2

      if (lengthSq > this._radiusSq) {
        this._particles[i * 2] *= this._radiusSq / lengthSq
        this._particles[i * 2 + 1] *= this._radiusSq / lengthSq
      }

      // Cap to circle
      this._particleSpeeds[i * 2] = 0
      this._particleSpeeds[i * 2 + 1] = 0

    }

    geometry.addAttribute('position', new THREE.BufferAttribute(this._positions, 2))
    geometry.addAttribute('color', new THREE.BufferAttribute(this._colors, 4))

    return new THREE.LineSegments(geometry, material)

  }

  static vertexShader = `
    precision mediump float;
    precision mediump int;
    uniform mat4 modelViewMatrix;
    uniform mat4 projectionMatrix;
    attribute vec2 position;
    attribute vec4 color;
    varying vec4 vColor;
    void main()	{
      vColor = color;
      gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 0.0, 1.0 );
    }
  `

  static fragmentShader = `
    precision mediump float;
    precision mediump int;
    varying vec4 vColor;
    void main()	{
      gl_FragColor = vColor;
    }
  `

}