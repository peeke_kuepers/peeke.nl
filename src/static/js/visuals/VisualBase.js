import observer from 'util/observer';
import { throttle } from 'util/eventHelpers'
import VisibilityMonitor from 'monitors/VisibilityMonitor';
import THREE from 'proxy/three';

class VisualBase {

    constructor(container, options = { simulationFrame: 16, near: .1, far: 3000, fov: 45 }) {

        this.__container = container;
        this.__options = options;
        this.__running = null;
        this.__time = performance.now();
        this.__simulationDebt = 0;
        this.__simulationFrame = options.simulationFrame;
        this.__renderer = null;
        this.__scene = null;
        this.__camera = null;
        this.__cameras = { perspective: null, orthographic: null };
        this.__monitor = new VisibilityMonitor(container);

        // Bind stuff
        this.__update = this.__update.bind(this);
        this.__onResize = throttle(this.__onResize.bind(this), 100);
        this.__onVisibilityChange = this.__onVisibilityChange.bind(this);
        this.__onExternalChange = this.__onExternalChange.bind(this);

        this._setup();
        this._resize();

    }

    pause() {

        if (!this.__running) return;

        this.__running = false;

    }

    run() {

        if (this.__running) return;

        this.__running = true;
        this.__time = performance.now();
        requestAnimationFrame(this.__update);

    }

    reveal() {
        throw new Error('reveal() not implemented');
    }

    hide() {
        throw new Error('hide() not implemented');
    }

    // dt = deltaTime (s)
    _simulate(dt) {
        throw new Error('_simulate(dt) not implemented');
    }

    _render() {
        throw new Error('_render(dt) not implemented');
    }

    _setup() {

        this.__renderer = new THREE.WebGLRenderer({ antialias: true, alpha: true });
        this.__scene = new THREE.Scene();

        this.__cameras = {
            perspective: new THREE.PerspectiveCamera(this.__options.fov, 1, this.__options.near, this.__options.far),
            orthographic: new THREE.OrthographicCamera(0, 0, 0, 0)
        };

        window.addEventListener('resize', this.__onResize);
        window.addEventListener('scroll', this.__onScroll, { passive: true });
        window.addEventListener('visibilitychange', this.__onVisibilityChange, { passive: true });
        observer.subscribe(this.__monitor, 'visibilitychange', this.__onExternalChange);
        
        this.__container.appendChild(this.__renderer.domElement);

    }

    _destroy() {

        this.__container.removeChild(this.__renderer.domElement);

        window.removeEventListener('resize', this.__onResize);
        window.removeEventListener('scroll', this.__onScroll, { passive: true });
        window.removeEventListener('visibilitychange', this.__onVisibilityChange, { passive: true });

        this.pause();

    }

    __update() {

        if (!this.__running) return;

        const now = performance.now();
        this.__simulationDebt = this.__simulationDebt + now - this.__time;
        this.__time = now;

        while(this.__simulationDebt >= this.__simulationFrame) {
            this.__simulationDebt -= this.__simulationFrame;
            // todo: should simulate live in webworker?
            this._simulate(this.__simulationFrame / 1000);
        }

        this._render();

        requestAnimationFrame(this.__update);

    }

    __onResize() {
        this._resize();
    }

    __onVisibilityChange() {
        this.__onExternalChange(!document.hidden);
    }
    
    __onExternalChange(bool) {
        if (this.__running === null) return;
        bool ? this.run() : this.pause();
    }

    _resize() {

        console.log('resize')

        const [width, height] = this.size;

        if (this.__camera === this.__cameras.perspective) {

            this.__cameras.perspective.aspect = width / height;
            this.__cameras.perspective.position.z = this.vmin / 2 / Math.tan(Math.PI / 180 * this.__options.fov / 2);

        } else if (this.__camera === this.__cameras.orthographic) {

            this.__cameras.orthographic.left = width / -2;
            this.__cameras.orthographic.right = width / 2;
            this.__cameras.orthographic.top = height / 2;
            this.__cameras.orthographic.bottom = height / -2;
            this.__cameras.orthographic.position.z = this.vmin;

        }

        this.__camera.updateProjectionMatrix();
        this.__renderer.setPixelRatio(window.devicePixelRatio);
        this.__renderer.setSize(width, height);

    }

    get size() {

        if (!this.__renderer.domElement) return [0, 0];

        const width = window.innerWidth;
        const height = window.innerHeight;

        return [width, height];

    }

    get vmin() {
        return Math.min(...this.size);
    }

    get vmax() {
        return Math.max(...this.size);
    }

    get element() {
        return this.__renderer.domElement;
    }
    
    get renderer() {
        return this.__renderer;
    }

    get scene() {
        return this.__scene;
    }

    get perspectiveCamera() {
        return this.__cameras.perspective;
    }

    get orthographicCamera() {
        return this.__cameras.orthographic;
    }

    get camera() {
        return this.__camera || this.__cameras.perspective;
    }

    set camera(camera) {
        this.__camera = camera;
    }

}

export default VisualBase;
