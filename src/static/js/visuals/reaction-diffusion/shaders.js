const reactionDiffusion = `
uniform vec2 u_mouse;
uniform vec2 u_resolution;
uniform vec2 u_inv_resolution;
uniform sampler2D u_texture;
uniform sampler2D u_mask;
uniform sampler2D u_blurred_x;
uniform sampler2D u_blurred_y;
uniform float u_feed_rate;
uniform float u_kill_rate;

vec4 sample(vec2 uv){
  return texture2D(u_texture,uv);
}

vec4 sample(vec2 uv,vec2 o){
  vec2 no=o*u_inv_resolution;
  
  if(texture2D(u_mask, uv+no).x<.5){
    return texture2D(u_texture,uv);
  }
  
  return texture2D(u_texture,uv+no);
}

void main(){
  vec2 uv=gl_FragCoord.xy*u_inv_resolution.xy;
  vec4 sampled=sample(uv);

  float a = clamp(texture2D(u_blurred_x,uv).x - sampled.x * sampled.y * sampled.y + u_feed_rate * (1.0 - sampled.x), 0.0, 1.0);
  float b = clamp(texture2D(u_blurred_y,uv).y + sampled.x * sampled.y * sampled.y - (u_kill_rate + u_feed_rate) * sampled.y, 0.0, 1.0);
  
  float distance = length(gl_FragCoord.xy - u_mouse);
  if (distance < 32.0) {
    a = mix(1., a, distance / 32.0);
    b = mix(.0, b, distance / 32.0);
  }

  float maskValue = texture2D(u_mask, uv).x;
  if(maskValue<1.){
    a = mix(1.0, a, maskValue);
    b = mix(0., b, maskValue);
  }

  gl_FragColor=vec4(a, b, sampled.zw);
}
`

const twoStepBlur = `
uniform vec2 u_resolution;
uniform vec2 u_inv_resolution;
uniform sampler2D u_texture;
uniform vec2 u_dir;
uniform float u_radius;

void main() {
  vec2 uv=gl_FragCoord.xy*u_inv_resolution.xy;

	vec4 sum = vec4(0.0);
    
	float blur = u_radius / 2.0;
    
	// weights from: http://dev.theomader.com/gaussian-kernel-calculator/
    
	sum += texture2D(u_texture, uv - 2.0 * blur * u_dir * u_inv_resolution) * 0.06136;
	sum += texture2D(u_texture, uv - 1.0 * blur * u_dir * u_inv_resolution) * 0.24477;
	sum += texture2D(u_texture, uv) * 0.38774;
	sum += texture2D(u_texture, uv + 1.0 * blur * u_dir * u_inv_resolution) * 0.24477;
	sum += texture2D(u_texture, uv + 2.0 * blur * u_dir * u_inv_resolution) * 0.06136;

	gl_FragColor = vec4(sum.rgb, 1.0);
}
`

export const compute = { reactionDiffusion, twoStepBlur }

const basicFragment = `
precision mediump float;

uniform vec2 u_resolution;
uniform sampler2D u_texture;

void main() {
  vec2 uv = gl_FragCoord.xy / u_resolution.xy;
  gl_FragColor = texture2D(u_texture, uv);
}
`

const display = `
precision mediump float;

uniform vec3 u_color_a;
uniform vec3 u_color_b;
uniform vec3 u_color_c;

uniform vec2 u_resolution;
uniform sampler2D u_texture;
uniform sampler2D u_mask;

void main() {
  
  vec2 uv = gl_FragCoord.xy / u_resolution.xy;
  vec4 sampled = texture2D(u_texture, uv);

  float color = sampled.x - sampled.y;
  vec3 result = color < .5
    ? mix(u_color_a, u_color_b, smoothstep(0.1, .9, color / .5))
    : mix(u_color_b, u_color_c, smoothstep(0.3, .6, (color - .5) / .5));

  gl_FragColor = vec4(result, 1.0);

}
`

export const fragment = { basic: basicFragment, display }

const basicVertex = `
void main() {
  gl_Position = projectionMatrix * modelViewMatrix * vec4(position, 1.0);
}
`

export const vertex = { basic: basicVertex }