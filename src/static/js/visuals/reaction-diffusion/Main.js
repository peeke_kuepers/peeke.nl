import THREE from 'proxy/three'

import { compute, fragment } from 'visuals/reaction-diffusion/shaders'

import TexturePass from "three/TexturePass";
import clampNumber from "util/clampNumber";
import mapNumber from "util/mapNumber";
import noise from "vendor/josephg/noise";

import { makePlaybackObservable } from 'visuals/helpers'
let frame

function visual() {
  const rad = deg => (deg / 180) * Math.PI;
  const calculateViewportHeight = (perspectiveAngle, distance) => {
    return Math.tan(rad(perspectiveAngle / 2)) * distance * 2;
  };

  const width = Math.round(Math.min(window.innerWidth, window.innerHeight) * .67)
  const height = Math.round(Math.min(window.innerWidth, window.innerHeight) * .67);
  const canvas = document.createElement('canvas')
  canvas.width = width
  canvas.height = height
  canvas.style.width = width + 'px'
  canvas.style.height = height + 'px'
  canvas.style.borderRadius = '50%'

  const viewportHeight = calculateViewportHeight(75, 30);

  const texturesLoading = []

  const scene = new THREE.Scene();
  const camera = new THREE.PerspectiveCamera(75, width / height, 0.1, 1000);
  camera.position.z = 30;

  const renderer = new THREE.WebGLRenderer({ canvas, alpha: true });
  renderer.setPixelRatio(1);
  renderer.setSize(width, height);

  let maskTextureLoaded
  texturesLoading.push(new Promise(resolve => { maskTextureLoaded = resolve }))
  const maskTexture = new THREE.TextureLoader().load('/textures/generic/mask.png', maskTextureLoaded)

  const seed = Math.random() * 1000

  const data = new Uint8Array(4 * width * height).map((_, i) => {
    const x = Math.floor(i / 4) % width
    const y = Math.floor(i / 4 / width)
    return i % 4 !== 1
      ? 255
      : clampNumber(noise.perlin3(x / 50, y / 50, seed), 0, 1) * 255
  })

  let textureA = new THREE.DataTexture(data, width, height, THREE.RGBAFormat)
  let textureB = textureA.clone()
  textureA.needsUpdate = true
  textureB.needsUpdate = true
  
  const swapPass = new TexturePass(renderer, fragment.basic);
  
  const displayPass = new TexturePass(renderer, fragment.display, {
    u_mask: { value: maskTexture },
    u_color_a: { value: new THREE.Vector3(0 / 255, 115 / 255, 233 / 255 )},
    u_color_b: { value: new THREE.Vector3(210 / 255, 210 / 255, 210 / 255 )},
    u_color_c: { value: new THREE.Vector3(1, 1, 1)},
  });

  const blurX = 3
  const blurY = 1.4

  const feedA = .022
  const feedB = .090
  const killA = .065
  const killB = .065

  const twoStepBlurShaderXStep1 = new TexturePass(renderer, compute.twoStepBlur, {
    u_radius: { value: blurX },
    u_dir: { value: new THREE.Vector2(1, 0) }
  })

  const twoStepBlurShaderXStep2 = new TexturePass(renderer, compute.twoStepBlur, {
    u_radius: { value: blurX },
    u_dir: { value: new THREE.Vector2(0, 1) }
  })

  const twoStepBlurShaderYStep1 = new TexturePass(renderer, compute.twoStepBlur, {
    u_radius: { value: blurY },
    u_dir: { value: new THREE.Vector2(1, 0) }
  })

  const twoStepBlurShaderYStep2 = new TexturePass(renderer, compute.twoStepBlur, {
    u_radius: { value: blurY },
    u_dir: { value: new THREE.Vector2(0, 1) }
  })

  const reactionDiffusionPass = new TexturePass(renderer, compute.reactionDiffusion, {
    u_mouse: { value: new THREE.Vector2 },
    u_mask: { value: maskTexture },
    u_blurred_x: { value: new THREE.Texture() },
    u_blurred_y: { value: new THREE.Texture() },
    u_feed_rate: { value: 0 },
    u_kill_rate: { value: 0 }
  });

  const geometry = new THREE.PlaneGeometry(
    viewportHeight * width / height,
    viewportHeight,
    32
  );

  const material = new THREE.MeshBasicMaterial();
  const plane = new THREE.Mesh(geometry, material);
  scene.add(plane);

  const light = new THREE.PointLight(0xffffff, 1, 100);
  light.position.set(20, 10, 30);
  scene.add(light);

  window.addEventListener('mousemove', e => {
    const rect = canvas.getBoundingClientRect()
    const mouseX = e.clientX - rect.left
    const mouseY = (window.innerHeight - e.clientY) - rect.top
    reactionDiffusionPass.uniforms.u_mouse.value = new THREE.Vector2(mouseX, mouseY)
  })

  const render = () => {
    
    // Do the gpu computation
    const blurredX = twoStepBlurShaderXStep2.process(twoStepBlurShaderXStep1.process(textureA))
    const blurredY = twoStepBlurShaderYStep2.process(twoStepBlurShaderYStep1.process(textureA))
    
    const f = noise.perlin2(performance.now() / 2000, seed)
    const feed = mapNumber(f, -1, 1, feedA, feedB)
    const kill = mapNumber(f, -1, 1, killA, killB)

    reactionDiffusionPass.uniforms.u_blurred_x.value = blurredX
    reactionDiffusionPass.uniforms.u_blurred_y.value = blurredY
    reactionDiffusionPass.uniforms.u_feed_rate.value = feed
    reactionDiffusionPass.uniforms.u_kill_rate.value = kill
    const reactionTexture = reactionDiffusionPass.process(textureA)

    // Swap
    textureB = reactionTexture
    textureA = swapPass.process(textureB)

    material.map = displayPass.process(textureB);
    renderer.render(scene, camera);
  };

  window.render = render
  
  const loop = () => {
    render();
    frame = requestAnimationFrame(loop);
  }
  
  const start = () => {
    Promise.all(texturesLoading).then(loop);
  }
  
  const pause = () => {
    cancelAnimationFrame(frame);
  }

  return {
    start, 
    pause,
    canvas
  }
}

export default class Controller {
  constructor(container, options = {}) {
    const {
      start, 
      pause,
      canvas
    } = visual()

    const playbackObservable = makePlaybackObservable(container)

    playbackObservable.forEach(state => {
      state === 'PLAY' && start()
      state === 'PAUSE' && pause()
    })
    
    container.appendChild(canvas)
    start()
  }
}