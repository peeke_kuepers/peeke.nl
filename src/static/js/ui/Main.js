import SubscribeButton from 'ui/SubscribeButton'

class Main {

  constructor (element) {

    const subscribeButton = element.querySelector('[data-subscribe]')

    subscribeButton && new SubscribeButton(subscribeButton)

  }

}

export default Main