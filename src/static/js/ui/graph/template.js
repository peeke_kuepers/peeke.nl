import idGenerator from 'util/idGenerator';

const defaultState = {
    range: [],
    domain: [],
    fns: [],
    colors: [],
    legend: false
};

const render = {

    range(range) {

        return range
            .slice()
            .reverse()
            .map(n => `<span>${ n }</span>`)
            .join('\n');

    },

    domain(domain) {

        return domain
            .map(n => `<span>${ n }</span>`)
            .join('\n');

    },

    legend(state) {

        if (!state.legend) return '';

        return `
            <div class="c-graph-legend">
                ${ this.legendItem(state.fns, state.colors) }
            </div>`;

    },

    legendItem(fns, colors) {

        return fns
            .map((fn, i) => `<div style="background-color: ${ colors[i] || colors[0] };">${ fn }</div>`)
            .join('\n');

    }

};


function graphTemplate(state) {

    state = Object.assign(defaultState, state);

    return `

        <div class="c-graph-container">
            <canvas class="c-graph-canvas"></canvas>
            <div class="c-graph-range">
                ${ render.range(state.range) }
                <div class="c-graph-range-label">
                    ${ state.rangeLabel }
                </div>
            </div>
            <div class="c-graph-domain">
                ${ render.domain(state.domain) }
                <div class="c-graph-domain-label">
                    ${ state.domainLabel }
                </div>
            </div>
        </div>
        
        ${ render.legend(state) }`;

}

export default graphTemplate;
