// todo: maybe refactor later?

import template from 'ui/graph/template';
import observer from 'util/observer';
import VisibilityMonitor from 'monitors/VisibilityMonitor';
import { makeRange } from 'math/mathHelpers';
import parseOptions from 'util/parseOptions';
import Timeline from 'motion/Timeline';
import { raf } from 'util/eventHelpers'

class Graph {

    constructor(element) {


        
        this._element = element;
        this._options = parseOptions(Graph, element);

        this._resize = this._resize.bind(this);
        this._render = this._render.bind(this);
        this._updateRendering = this._updateRendering.bind(this);
        this._onVisibilityChange = this._onVisibilityChange.bind(this);

        this._monitor = new VisibilityMonitor(element);
        this._elements = this._createElements();
        this._elements.inputs = Array.from(document.querySelectorAll(this._options.inputs));

        this._bindEvents();
        this._resize();

        this.fns = this._options.fns;

        this._ctx = this._elements.canvas.getContext('2d');

        this._animation = null;

        this._updateRendering();

    }
    
    static get options() {
        return {
            range: [0, 1],
            rangeLabel: 'output',
            domain: [0, 1],
            domainLabel: 'input',
            fns: [],
            resolution: 2, // ideal, but less performant would be 1, higher values are generally okay for less bendy graphs
            animated: false,
            legend: true,
            lowlight: '#999',
            ruler: false,
            colors: ['#ddd'],
            inputs: null
        }
    }

    static get parse() {
        return {
            domain: s => s.split(',').map(s => +s.trim()),
            domainLabel: s => s,
            range: s => s.split(',').map(s => +s.trim()),
            rangeLabel: s => s,
            fns: s => s.split(';').map(s => s.trim()),
            resolution: s => +s,
            animated: s => s === 'true',
            legend: s => s === 'true',
            lowlight: s => s,
            ruler: s => s,
            colors: s => s.split(','),
            inputs: s => s
        }
    }

    run() {

        if (!this._options.animated) return;
        if (this._running) return;

        this._running = true;
        this._render();

    }

    pause() {

        if (!this._running) return;

        this._running = false;
        this._animation && this._animation.cancel();

    }

    _onVisibilityChange(visible) {
        visible ? this.run() : this.pause();
    }

    _createElements() {

        this._element.innerHTML = template(this._options);

        return {
            container: this._element.querySelector('.c-graph-container'),
            canvas: this._element.querySelector('.c-graph-canvas'),
            range: this._element.querySelector('.c-graph-range'),
            domain: this._element.querySelector('.c-graph-domain'),
            rangeEntries: Array.from(this._element.querySelectorAll('.c-graph-range span')),
            domainEntries: Array.from(this._element.querySelectorAll('.c-graph-domain span')),
            legendEntries: Array.from(this._element.querySelectorAll('.c-graph-legend div'))
        };

    }

    _resize() {

        const [ width, height ] = this.size;

        this._elements.canvas.width = width;
        this._elements.canvas.height = height;

        this.range = this._options.range;
        this.domain = this._options.domain;

    }

    _bindEvents() {

        this._elements.inputs.forEach(input => {
            input.addEventListener('input', () => {
                this.fns = this._options.fns;
                this._updateRendering();
            });
        });

        window.addEventListener('resize', raf(this._resize));

        observer.subscribe(this._monitor, 'visibilitychange', this._onVisibilityChange);

    }
    
    _render() {
        
        if (!this._running) return;

        this._updateRendering();
        this._animation = new Timeline(1000, 5000, this._updateRendering);
        this._animation.then(() => setTimeout(this._render, 1000));

    }

    _updateRendering(t = 0) {

        const [ width, height ] = this.size;
        const resolution = window.devicePixelRatio * this._options.resolution;

        const draw = (start, stop, overrideColor, lineWidth) => (fn, i = 0) => {

            const color = overrideColor ? overrideColor : this._options.colors[i] || this._options.colors[0];

            this._ctx.strokeStyle = color;
            this._ctx.lineWidth = (lineWidth || 1) * window.devicePixelRatio;
            this._ctx.beginPath();
            this._ctx.moveTo(start, this._rangeToY(fn(this._nToDomain(start))));

            new Array(Math.ceil((stop - start) / resolution) + 1)
                .fill(0)
                .forEach((_, n) => {
                    n *= resolution;
                    const valueX = this._nToDomain(n + start);
                    this._ctx.lineTo(n + start, this._rangeToY(fn(valueX)));
                });

            this._ctx.stroke();
            this._ctx.closePath();

        };

        this._ctx.clearRect(0, 0, width, height);

        if (this._options.animated) {
            const fn = this._fns.slice(-1)[0];
            this._options.ruler && draw(0, width, this._options.ruler)(_ => fn(this._nToDomain(t * width)));
            this._fns.slice(0, this._fns.length - 1).forEach(draw(0, width));
            this._fns.slice(-1).forEach(draw(0, t * width, this._options.colors.slice(-1)[0], 2));
            this._fns.slice(-1).forEach(draw(t * width, width, this._options.lowlight));
        } else {
            this._fns.forEach(draw(0, width));
        }

    }

    set fns(expressions) {

        const setValues = (result, input) => {
            const regex = new RegExp(`\\$\\{\\s${ input.name }\\s\\}`, 'g');
            return result.replace(regex, input.value);
        };

        const parseExpression = expr => this._elements.inputs.reduce(setValues, expr);

        const parsedExpressions = expressions
            .map(parseExpression);

        this._fns = parsedExpressions.map(expr => new Function('t', `return ${ expr }`));

        this._elements.legendEntries.forEach((elem, i) => elem.innerHTML = parsedExpressions[i]);

    }

    set range(range) {
        this._rangeToY = makeRange(range[0], range.slice(-1)[0], this.size[1] - 2, 1);
    }

    set domain(domain) {
        this._nToDomain = makeRange(0, this.size[0], domain[0], domain.slice(-1)[0]);
    }

    get size() {
        return [
            this._elements.container.offsetWidth,
            this._elements.container.offsetHeight
        ].map(d => d * window.devicePixelRatio);
    }

}

export default Graph;