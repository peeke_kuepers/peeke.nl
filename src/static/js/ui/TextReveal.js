const reflow = element => element.offsetTop

class TextReveal {

	constructor(element) {

		this._element = element;
		this._options = Object.assign(TextReveal.options, (element.dataset || {}));

		element.innerHTML = element.innerHTML.replace(/(?=\S*-)([a-zA-Z-]+)/, v => `<span style="white-space: nowrap;">${ v }</span>`);
		this._html = element.innerHTML;

		const spans = this._spanUp(element);
		setTimeout(() => this._reveal(spans), +this._options.delay);

		this._element.setAttribute('data-interactive', '');

	}

	static get options() {
		return {
			duration: 700,
			interval: 75,
			delay: 200,
			cssEase: 'cubic-bezier(0.165, 0.84, 0.44, 1)',
			reverse: false
		};
	}

	_spanUp(element) {

		const text = element.textContent.trim();
		const words = text.split(' ');
		const fragment = document.createDocumentFragment();
		const spans = [];

		words.forEach((word, i) => {

			const outerSpan = document.createElement('span');
			outerSpan.style.display = 'inline-block';
			outerSpan.style.padding = '1rem';
			outerSpan.style.paddingTop = '0';
			outerSpan.style.margin = '-1rem';
			outerSpan.style.marginTop = '0';
			outerSpan.style.overflow = 'hidden';

			const span = document.createElement('span');
			span.innerHTML = word;
			span.style.display = 'inline-block';
			span.style.opacity = '0';
			span.style.transform = 'translateY(100%) translateY(2.5rem)';
			span.style.transition = `${ +this._options.duration }ms ${ i * +this._options.interval }ms ${this._options.cssEase}`;

			spans.push(span);
			outerSpan.appendChild(span);
			fragment.appendChild(outerSpan);
			fragment.appendChild(document.createTextNode(' '));

		});

		element.innerHTML = '';
		element.appendChild(fragment);

		reflow(element);

		return spans;

	}

	_reveal(spans) {

		spans.forEach(span => {
			span.style.opacity = '';
			span.style.transform = '';
		});

		const maxDuration = +this._options.duration + +this._options.interval * spans.length;

		setTimeout(() => {
			this._element.innerHTML = this._html;
		}, maxDuration);

	}

}

export default TextReveal;
