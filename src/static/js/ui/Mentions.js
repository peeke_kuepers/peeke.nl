import VisibilityMonitor from 'monitors/VisibilityMonitor'
import observer from 'util/observer'

class Mentions {

  constructor (element) {

    this._element = element
    this._monitor = new VisibilityMonitor(element)
    this._onVisibilityChange = this._onVisibilityChange.bind(this)

    observer.subscribe(this._monitor, 'visibilitychange', this._onVisibilityChange)

  }

  _onVisibilityChange (visible) {
    visible && this._loadMentions()
  }

  _loadMentions () {

    observer.unsubscribe(this._monitor, 'visibilitychange', this._onVisibilityChange)
    this._element.setAttribute('data-state', 'loading')

    fetch('/webmentions/' + encodeURIComponent(window.location.href))
      .then(response => response.text())
      .then(text => (new DOMParser()).parseFromString(text, 'text/html'))
      .then(doc => doc.querySelector('.l-mentions').innerHTML)
      .then(html => this._element.innerHTML = html)
      .then(() => requestAnimationFrame(() => this._element.setAttribute('data-state', 'complete')))

  }

}

export default Mentions