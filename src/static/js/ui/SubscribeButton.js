import NotificationManager from 'util/notificationManager';

class SubscribeButton {

	constructor(element) {

		this._element = element;
		this._element.removeAttribute('hidden');

		if (Notification.permission === 'default') {
			this._initialize();
		} else if (Notification.permission === 'denied') {
			element.innerHTML = 'You`ve disabled push notifications :`('
		} else {
			element.innerHTML = 'You are subscribed to push notifications, good for you!'
		}

	}
	
	_initialize() {

		this._onSubscribe = this._onSubscribe.bind(this);

		this._element.addEventListener('click', this._onSubscribe)

	}

	_onSubscribe(e) {

		e.preventDefault();

    NotificationManager.subscribeToNotications()
			.then(() => this._element.setAttribute('data-subscribed', 'true'));
		
	}

}

export default SubscribeButton;