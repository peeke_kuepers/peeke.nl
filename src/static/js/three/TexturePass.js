import THREE from 'proxy/three'

class TexturePass {
  constructor(renderer, fragmentShader, uniforms = {}) {
    this.renderer = renderer;
    this.scene = new THREE.Scene();

    const { width, height } = renderer.getDrawingBufferSize();

    this.camera = new THREE.OrthographicCamera(
      width / -2,
      width / 2,
      height / 2,
      height / -2,
      -1,
      1
    );

    this.scene.add(this.camera);

    this.renderTarget = new THREE.WebGLRenderTarget(width, height);

    const texture = new THREE.Texture()
    texture.repeat.set(1, 1)

    this.material = new THREE.ShaderMaterial({
      uniforms: {
        u_resolution: {
          value: new THREE.Vector2(width, height)
        },
        u_inv_resolution: {
          value: new THREE.Vector2(1 / width, 1 / height)
        },
        u_texture: {
          value: texture
        },
        ...uniforms
      },
      vertexShader: `void main() {
        gl_Position = projectionMatrix * modelViewMatrix * vec4(position, 1.0);
      }`,
      fragmentShader
    });

    const geometry = new THREE.PlaneGeometry(width, height);
    const mesh = new THREE.Mesh(geometry, this.material);
    this.scene.add(mesh);
  }

  get uniforms() {
    return this.material.uniforms;
  }

  process(texture) {
    this.material.uniforms.u_texture.value = texture;
    this.renderer.render(this.scene, this.camera, this.renderTarget);
    return this.renderTarget.texture;
  }
}

export default TexturePass;
