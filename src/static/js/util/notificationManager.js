const error = error => {
  throw error
}

class NotificationManager {

  static subscribeToNotications () {

    return this._getNotificationPermission()
      .then(() => this._getPushManager())
      .then(pm => this._registerSubscription(pm))
      .then(sub => this._persistSubscription(sub))
      .catch(error)

  }

  static _getNotificationPermission () {

    return new Promise((resolve, reject) => {
      Notification.requestPermission(result => result === 'granted' ? resolve() : reject())
    })

  }

  static _getPushManager () {
    return navigator.serviceWorker.ready
      .then(registration => registration.pushManager)
  }

  static _registerSubscription (pushManager) {

    return pushManager.getSubscription().then(subscription => {

      if (subscription) {
        return Promise.reject(new Error('Already subscribed'))
      }

      return pushManager.subscribe({
        applicationServerKey: window.vapidPublicKey,
        userVisibleOnly: true
      })

    })

  }

  static _persistSubscription (subscription) {

    const fetchOptions = {
      method: 'POST',
      headers: new Headers({
        'Content-Type': 'application/json'
      }),
      body: JSON.stringify(subscription)
    }

    console.log('subscription -->', subscription)

    return fetch('/api/subscription/create', fetchOptions)

  }

}

export default NotificationManager