function px(n) {
	return window.devicePixelRatio * n;
}

function rad(deg) {
	return Math.PI / 180 * deg;
}

function deg(rad) {
	return rad * 180 / Math.PI;
}

export { px, rad, deg }
