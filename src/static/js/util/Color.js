class Color {

	static blend(color1, color2, f = .5) {
		return color1.map((facet, i) => facet * f + color2[i] * (1 - f));
	}


	static gradient(colors, position) {

		position = position % 1;

		let c1 = colors[Math.floor(position * (colors.length - 1))],
		 	c2 = colors[Math.ceil(position * (colors.length - 1))];

		return Color.blend(c1, c2, position * (colors.length  - 1) % 1);

	}
}

export default Color;