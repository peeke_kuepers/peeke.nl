class IdGenerator {

    constructor() {
        this._n = 0;
    }

    getId() {
        return '_gen-id-' + (this._n++);
    }

}

export default new IdGenerator();
