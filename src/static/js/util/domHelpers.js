import { bounds } from 'math/mathHelpers';

function scrollProgression(element, scrollParent) {
	let rct = element.getBoundingClientRect();
	scrollParent = scrollParent || window;
	return bounds(-rct.top / (element.offsetHeight - scrollParent.innerHeight), 0, 1);
}

function getInputValue(element) {

	if (['checkbox', 'radio'].indexOf(element.type) !== -1) {
		return element.checked ? element.value : '';
	}

	if (element.tagName.toLowerCase() === 'option') {
		return element.parentNode.value;
	}

	return element.value;

}

function setInputValue(element, value) {

	if (['checkbox', 'radio'].indexOf(element.type) !== -1) {
		element.checked = element.value === value;
		return;
	}

	if (element.tagName.toLowerCase() === 'option') {
		element.parentNode.value = value;
		return;
	}

	element.value = value;

}

function nodeFromHtml(html) {
    const parent = document.createElement('div');
    parent.innerHTML = html;
    return parent.children[0];
}


export { scrollProgression, getInputValue, setInputValue, nodeFromHtml };