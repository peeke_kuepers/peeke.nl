import observer from 'util/observer';

class Measures {

	constructor(element) {

		this._element = element;
		
		this._throttle = false;
		this._mouse = {x: 0, y: 0};

		this._bindEvents();
		this._updateMeasures();

	}

	get pr() {
		return this._pr;
	}

	get width() {
		return this._width;
	}

	get height() {
		return this._height;
	}

	get min() {
		return this._min;
	}

	get max() {
		return this._max;
	}

	get center() {
		return {
			x: this._width / 2,
			y: this._height / 2
		}
	}

	get mouse() {
		return this._mouse;
	}

	get mouseX() {
		return this._mouse.x;
	}

	get mouseY() {
		return this._mouse.y;
	}

	_bindEvents() {
		this._onResize = this._onResize.bind(this);
		this._onMouseMove = this._onMouseMove.bind(this);
		window.addEventListener('resize', this._onResize);
		document.documentElement.addEventListener('mousemove', this._onMouseMove);
		document.documentElement.addEventListener('touchmove', this._onMouseMove);
	}

	_onResize() {

		if (!this._throttle) {
			this._updateMeasures();
			this._throttle = setTimeout(() => {
				this._updateMeasures();
				this._throttle = false;
			}, 16);
		}

	}

	_onMouseMove(e) {
		this._mouse = {
			x: e.pageX || e.touches[0].pageX || 0,
			y: e.pageY || e.touches[0].pageY || 0
		};
	}

	_updateMeasures() {
		this._width = window.innerWidth;
		this._height = window.innerHeight;
		this._min = this._width > this._height ? this._height : this._width;
		this._max = this._width > this._height ? this._height : this._width;
		observer.publish(this, 'measureschange');
	}

}

export default new Measures();
