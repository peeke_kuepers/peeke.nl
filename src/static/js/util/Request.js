/**
 * Request class, using the (polyfilled) fetch API
 * @class
 */

const FETCH_HEADERS = [];

class Request {

	fetch(url, headers = []) {

		return new Promise((resolve, reject) => {

			let request = new XMLHttpRequest();

			request.open('GET', url);

			request.onload = function() {
				if (request.status === 200) {
					resolve(request.response || request.responseText);
				} else {
					reject(Error('error code:' + request.statusText));
				}
			};
			request.onerror = function() {
				reject(Error('There was a network error.'));
			};

			Object.keys(headers).forEach(key => request.setRequestHeader(key, headers[key]));

			request.send();

		});

	}

	/**
	 * Fetch JSON data from an URL
	 * @param {string} url - The resource URL
	 * @returns {Promise} - A Promise, resolving with the parsed JSON data
	 */
	fetchJSON(url) {
		return this.fetch(url, FETCH_HEADERS).then(r => JSON.parse(r));
	}

	/**
	 * Fetch plain text data from an URL
	 * @param {string} url - The resource URL
	 * @returns {Promise} - A Promise, resolving with the plain text
	 */
	fetchPlainText(url) {
		return this.fetch(url, FETCH_HEADERS);
	}

}

export default new Request();
