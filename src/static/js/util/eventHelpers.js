function throttle(fn, ms) {

	let throttling = false,
		timeout;

	return function _throttle() {
		if (!throttling) {

			throttling = true;
			timeout = setTimeout(() => throttling = false, ms);

			fn.apply(arguments);

		}
	}

}

function debounceHead(fn, ms) {

    let timeout;

    return function _debounce() {

        if (!timeout) {
            fn.apply(null, arguments);
        }

        clearTimeout(timeout);
        timeout = setTimeout(() => timeout = false, ms);

    };

}

function debounceTail(fn, ms) {

    let timeout;

    return function _debounce() {
        clearTimeout(timeout);
        timeout = setTimeout(() => fn.apply(null, arguments), ms);
    };

}

function raf(fn) {

	let raf;

	return function _raf() {
		cancelAnimationFrame(raf);
		raf = requestAnimationFrame(() => fn.apply(null, arguments));
	}

}

function dispatchEvent(source, event, options = {}) {

    console.log('dispatch', event, 'from', source, options);

    const defaultOptions = {
        bubbles: true,
        cancelable: true
    };

    source.dispatchEvent(new CustomEvent(event, Object.assign(options, defaultOptions)));

}

export { throttle, debounceHead, debounceTail, raf, dispatchEvent };
