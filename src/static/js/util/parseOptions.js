const parseOptions = (classReference, element) => {

    const options = Object.keys(element.dataset)
        .reduce((result, key) => {

            result[key] = classReference.parse[key]
                ? classReference.parse[key](element.dataset[key])
                : element.dataset[key];

            return result;
            
        }, {});

    return Object.assign(classReference.options, options);

};

export default parseOptions;