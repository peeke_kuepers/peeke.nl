function mod (x, n) {
  return (x % n + n) % n
}

function bounds (x, left, right) {
  if (left > right) {
    [left, right] = [right, left]
  }
  return Math.max(left, Math.min(right, x))
}

function mapToRange (n, inputLeft, inputRight, outputLeft, outputRight) {
  return (n - inputLeft) / (inputRight - inputLeft) * (outputRight - outputLeft) + outputLeft
}

function makeRange (inputLeft, inputRight, outputLeft, outputRight) {
  return n => mapToRange(n, inputLeft, inputRight, outputLeft, outputRight)
}

function distance (point1, point2) {

  point1.z = point1.z || 0
  point2.z = point2.z || 0

  return Math.sqrt(
    Math.pow(point1.x - point2.x, 2) +
    Math.pow(point1.y - point2.y, 2) +
    Math.pow(point1.z - point2.z, 2)
  )

}

function diff (point1, point2) {

  const d = {
    x: point1.x - point2.x,
    y: point1.y - point2.y,
  }

  if (typeof point1.z !== 'undefined' && typeof point2.z !== 'undefined') {
    d.z = point1.z - point2.z
  }

  return d

}

function angle (point1, point2) {
  return Math.atan2(point2.y - point1.y, point2.x - point1.x)
}

function circum (r) {
  return .5 * Math.PI * r
}

function circle (center, theta, radius) {
  return {
    x: center.x + radius * Math.cos(theta),
    y: center.y - radius * Math.sin(theta)
  }
}

function sum (arr) {
  return arr.reduce((res, val) => res + val, 0)
}

function min (arr) {
  return arr.reduce((res, val) => Math.min(res, val), Infinity)
}

function point (x, y, z) {

  const point = {x, y}

  if (typeof z !== 'undefined') {
    point.z = z
  }

  return point

}

function unit (vec) {

  if (typeof vec.z !== 'undefined') {
    const i = 1 / Math.sqrt(vec.x * vec.x + vec.y * vec.y + vec.z * vec.z)
    return {
      x: vec.x * i,
      y: vec.y * i,
      z: vec.z * i
    }
  }

  const i = 1 / Math.sqrt(vec.x * vec.x + vec.y * vec.y)
  return {
    x: vec.x * i,
    y: vec.y * i
  }

}

function dot (vec1, vec2) {
  let dot = vec1.x * vec2.x + vec1.y * vec2.y
  if (typeof vec1.z !== 'undefined' && typeof vec2.z !== 'undefined') {
    dot += vec1.z * vec2.z
  }
  return dot
}

function multiply (vec, factor) {
  let rvec = {
    x: vec.x * factor,
    y: vec.y * factor
  }

  if (typeof vec.z !== 'undefined') {
    rvec.z = vec.z * factor
  }

  return rvec
}

function subtract (vec1, vec2) {
  let vec = {
    x: vec1.x - vec2.x,
    y: vec1.y - vec2.y
  }
  if (typeof vec1.z !== 'undefined' && typeof vec2.z !== 'undefined') {
    vec.z = vec1.z - vec2.z
  }
  return vec
}

function reverse (vec) {
  return multiply(vec, -1)
}

function reflect (vec1, vec2) {
  // r = a - 2<a, n> n
  vec2 = unit(vec2)
  return subtract(vec1, multiply(vec2, dot(vec1, vec2) * 2))
}

function abs (value) {
  return value < 0 ? -value : value
}

function moveDistanceFromPoint (point1, point2, distance) {

  const d = diff(point1, point2)
  const u = unit(d)
  return multiply(u, distance)

}

export {
  mod,
  bounds,
  mapToRange,
  makeRange,
  multiply,
  diff,
  distance,
  angle,
  circum,
  circle,
  sum,
  min,
  point,
  moveDistanceFromPoint,
  reflect,
  reverse,
  unit,
  abs
}