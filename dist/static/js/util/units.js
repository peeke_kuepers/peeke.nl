"use strict";System.register([],function(t,e){function n(t){return window.devicePixelRatio*t}function r(t){return Math.PI/180*t}function i(t){return 180*t/Math.PI}return{setters:[],execute:function(){t("px",n),t("rad",r),t("deg",i)}}});
//# sourceMappingURL=units.js.map
