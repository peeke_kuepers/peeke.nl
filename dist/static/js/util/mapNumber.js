"use strict";System.register(["util/clampNumber"],function(t,e){var u,r;return{setters:[function(t){u=t.default}],execute:function(){r=function(t,e,r,n,c){var i=e<r?u(t,e,r):u(t,r,e),f=r-e,s=c-n;return(i-e)/f*s+n},t("default",r)}}});
//# sourceMappingURL=mapNumber.js.map
