"use strict";System.register([],function(t,e){var r;return{setters:[],execute:function(){r=function(t,e){var r=Object.keys(e.dataset).reduce(function(r,s){return r[s]=t.parse[s]?t.parse[s](e.dataset[s]):e.dataset[s],r},{});return Object.assign(t.options,r)},t("default",r)}}});
//# sourceMappingURL=parseOptions.js.map
