"use strict";System.register(["ui/SubscribeButton"],function(t,e){function n(t,e){if(!(t instanceof e))throw new TypeError("Cannot call a class as a function")}var r,u;return{setters:[function(t){r=t.default}],execute:function(){u=function t(e){n(this,t);var u=e.querySelector("[data-subscribe]");u&&new r(u)},t("default",u)}}});
//# sourceMappingURL=Main.js.map
