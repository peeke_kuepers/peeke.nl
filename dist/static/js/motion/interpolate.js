"use strict";System.register([],function(t,r){var e;return{setters:[],execute:function(){e=function(t,r){var e=arguments.length>2&&void 0!==arguments[2]?arguments[2]:1,n=Math.round((r-t)/e),u=n?Math.abs(n)/n:0;return Array(Math.abs(n)+1).fill(t).map(function(t,r){return t+e*r*u})},t("default",e)}}});
//# sourceMappingURL=interpolate.js.map
